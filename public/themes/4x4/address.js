$(document).ready(function(){

})
function editAddress(el) {
    var id = el.attr('data-id');
    var name = el.parent().siblings('.name').text().trim();
    var county = el.parent().siblings('.county').text().trim();
    var district = el.parent().siblings('.district').text().trim();
    var zipcode = el.parent().siblings('.zipcode').text().trim();
    var address = el.parent().siblings('.address').text().trim();
    $('input[name="address_id"]').val(id)
    $('input[name="name"]').val(name)
    $('input[name="county"]').val(county)
    $('input[name="district"]').val(district)
    $('input[name="zipcode"]').val(zipcode)
    $('textarea[name="address"]').val(address)
}

function addAddress(){
    $('input[name="address_id"]').val('');
    $('#address_form').submit()
}
