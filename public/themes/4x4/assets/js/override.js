(function(window, document){

    'use strict';

    if(typeof this == 'undefined'){
        return
    }

    var Events = this.events,
        Override = this.override.bind(this);

    this.setPredefinedModels([
        'ream.models.default',
        'ream.models.product',
        'ream.models.member'
    ]);

    this.utils.extend(this.config, {
    	controllerStateByOverrideRequireItems: {
    		'default': {
    			index: {
    				files: [
    					'stylesheets/layouts/list.css'
    				]
    			}
    		}
    	}
    });

	var jQWindow = $(window),
		jQDocument = $(document);

    var globalTemplateProperties = {
        rootPath: themePath + '/4x4/'
    }

	/* Helpers */
	var createSlider = function(items){
		for(var i in items) {
			ream.getComponent('layout').plugins.slider('#' + items[i] + '-product', {
				pagination: '.' + items[i] + '-product-pagination',
				paginationClickable: true,
				slidesPerView: 2,
				spaceBetween: 16,
				autoplay: 3000
			});
		}
	}

    /* Default Controller [Listener] */
    Events.addListener('ream:controllers:default', function(Scope){
    	Override('controller', {
    		properties: {
               templates: {
    				states: {
                        index: {
                            content: globalTemplateProperties
                        },
    					newProducts: {
    						content: globalTemplateProperties
    					},
    					rebateProducts: {
    						content: globalTemplateProperties
    					},
    					homeProducts: {
    						content: globalTemplateProperties
    					}
    				},
    				showcase: {
    					container: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });

	/* Product Controller [Listener] */
    Events.addListener('ream:controllers:product', function(Scope){
    	Override('controller', {
    		properties: {
    			templates: {
    				states: {
    					details: {
    						content: globalTemplateProperties
    					}
    				},
    				product: {
    					showcase: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });

    /* Category Controller [Listener] */
    Events.addListener('ream:controllers:category', function(Scope){
    	Override('controller', {
    		properties: {
    			templates: {
    				states: {
    					list: {
    						content: globalTemplateProperties
    					}
    				},
    				showcase: {
    					container: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });

    /* Search Controller [Listener] */
    Events.addListener('ream:controllers:search', function(Scope){
    	Override('controller', {
    		properties: {
    			templates: {
    				states: {
    					results: {
    						content: globalTemplateProperties
    					}
    				},
    				showcase: {
    					container: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });

    /* Brand Controller [Listener] */
    Events.addListener('ream:controllers:brand', function(Scope){
    	Override('controller', {
    		properties: {
    			templates: {
    				states: {
    					list: {
    						content: globalTemplateProperties
    					}
    				},
    				showcase: {
    					container: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });

	/* Default View*/
	Events.addListener('ream:views:index', function(Scope){
		Override('view', {
			methods: {
				render: function(){
                    var currentView = this;
                    var getShowcaseTemplate = function(method, response){
                        return currentView.actions.getTemplate({
                            path: 'default/carousel',
                            rootPath: themePath + '/templates/',
                            data: {
                                products: response[method]['products']
                            }
                        });
                    };
                    var getHomeTemplate = function(method, response){
						return currentView.actions.getTemplate({
							path: 'default/home',
							rootPath: themePath + '/templates/',
							data: {
								products: response[method]['products']
							}
						});
					};
                    $.when(
						ream.getModels('product').getNewProducts(),
                        ream.getModels('product').getRebateProducts(),
						ream.getModels('product').getHomeProducts()
                    ).done(function(){
                        var response = {
							newProducts: arguments[0][0],
							rebateProducts: arguments[1][0],
							homeProducts: arguments[2][0]
                        };
                        $.when(
							getShowcaseTemplate('newProducts', response),
							getShowcaseTemplate('rebateProducts', response),
							getHomeTemplate('homeProducts', response)
                        ).done(function(){
                            var templates = {
								news: arguments[0],
								rebate: arguments[1],
								home: arguments[2]
							};
                            ream.utils.extend(response, {
								newProducts: {
                                    template: templates.news,
                                    products: response.newProducts['products']
                                },
								rebateProducts: {
                                    template: templates.rebate,
                                    products: response.rebateProducts['products']
                                },
								homeProducts: {
                                    template: templates.home,
                                    products: response.homeProducts['products']
                                }
                            });
                            ream.utils.extend(currentView.templates['data'], response);
            			    currentView.actions.renderTemplate(currentView.defaultSelectors.content, 'html', currentView.templates, currentView.afterRender.bind(currentView));
                        });
        			});
                },
				afterPlugin: function(){
					createSlider(['news', 'rebate']);
					ream.getComponent('layout').setHeight('showcase');
				},
				afterRender: function(){
					ream.getComponent('layout').plugins.slider('#home-images', {
						pagination: '.home-images-pagination',
                        paginationClickable: true,
                        paginationBulletRender: function (index, className) {
                            return '<span class="' + className + '">' + (index + 1) + '</span>';
                        },
						autoplay: 3000
					});
					jQWindow.on('resize', function(){
						this.afterPlugin();
					}.bind(this));
					this.afterPlugin();
				},
			}
		}, Scope);
	});

	/* Product Details View*/
    Events.addListener('ream:controllers:productDetails', function(Scope){
    	Override('controller', {
    		properties: {
    			templates: {
    				showcase: {
    					similar: globalTemplateProperties
    				}
    			}
    		}
    	}, Scope);
    });
	Events.addListener('ream:views:productDetails', function(Scope){
		Override('view', {
			methods: {
				render: function(){
					var currentView = this;
					$.when(
						ream.getModels('banner').getList()
					).done(function(){
						var response = {
                            banners: arguments[0]['banners']
						};
						ream.utils.extend(currentView.templates['data'], response);
						currentView.actions.renderTemplate(currentView.defaultSelectors.content, 'html', currentView.templates, currentView.afterRender.bind(currentView));
					});
				},
				afterPlugin: function(){
					var defaultAfterPluginSelectors = {
						slider: {
							similar: {
								wrapper: '#similar-images',
								pagination: '.similar-images-pagination'
							},
							offered: {
								wrapper: '#offered-images',
								pagination: '.offered-images-pagination'
							}
						}
					}
					ream.getComponent('layout').plugins.slider(defaultAfterPluginSelectors.slider.similar.wrapper, {
						pagination: defaultAfterPluginSelectors.slider.similar.pagination,
						paginationClickable: true,
						slidesPerView: 2,
						spaceBetween: 16
					});
					ream.getComponent('layout').plugins.slider(defaultAfterPluginSelectors.slider.offered.wrapper, {
						pagination: defaultAfterPluginSelectors.slider.offered.pagination,
						paginationClickable: true,
						slidesPerView: 2,
						spaceBetween: 16
					});
					ream.getComponent('layout').setHeight('showcase');
				}
			}
		}, Scope);
	});

}.call(window.ream, window, document));
