ream.addComponent('globalComponent', function() {
	'use strict';

	var defaultSelectors = {
		body: 'body',
		loading: '.loading',
		wrapper: '#wrapper',
		header: '#header',
		footer: '#footer',
		panel: {
			overlay: '.overlay',
			input: '.panel input',
			top: '.panel-top > .panel-layout',
			left: '.panel-left > .panel-layout',
			right: '.panel-right > .panel-layout'
		},
		showcase: {
			div: '.showcase',
			image: '.showcase-image'
		}
	};

	var jQWindow = $(window),
		jQDocument = $(document);

	return {
		initialize: function(){
			ream.getComponent('globalComponent').eventListeners();
		},

		panel: {
			effect: {
				timeout: 300
			},
			template: {
				loading: '<div class="loading"><div class="loading-inner"><div class="circle"></div></div></div>'
			}
		},

		events: {
			'tap [data-action=cart-quantity-negative]' : 'cartQuantityNegative',
			'tap [data-action=cart-quantity-positive]' : 'cartQuantityPositive',
			'change [data-action=cart-select-quantity]': 'cartSelectQuantitiy',
			'tap [data-action=showCascadeListLevel]': 'showCascadeListLevel'
		},

		changeAmount: function(event, element, cartItemId, quantity){
			ream.get('controller', 'cart').done(function(CartController){
				CartController.actions.changeAmount({
					id: cartItemId,
					quantity: quantity,
					button: element,
					onComplete: function(response){
						if(response['no_stock']){
							element.attr('data-action', 'nostock').removeClass('button-primary').addClass('button-danger').html('Stokta Yok');
							ream.getComponent('header').cleanCacheFormCartProduct(cartItemId);
						}else{
							ream.getComponent('header').renderCart();
						}
					}
				});
			});
		},

		decimalStockTypes: ['m2', 'gram', 'kg', 'metre'],
		cartQuantityNegative: function(event, element) {
			var quantity = Number(element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').html()),
				stockAmount = Number(element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-stockamount')),
				stockType = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-stocktype'),
				cartItemId = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-cartItemId');
			if(this.decimalStockTypes.indexOf(stockType) != -1){
				if (quantity > 0.1) {
					quantity = quantity - Number(0.1);
				}
			} else {
				if (quantity > 1) {
					quantity = quantity - 1;
				}
			}
			$('.panel-right .panel-layout').append(ream.getComponent('globalComponent').panel.template.loading);
			ream.getComponent('globalComponent').changeAmount(event, element, cartItemId, quantity);
			element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').html(quantity.toFixed(1))
		},

		cartQuantityPositive: function(event, element) {
			var quantity = Number(element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').html()),
				stockAmount = Number(element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-stockamount')),
				stockType = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-stocktype'),
				stockTypeLanguage = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-language'),
				cartItemId = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-cartItemId');
			if (stockAmount > quantity) {
				if(this.decimalStockTypes.indexOf(stockType) != -1){
					quantity = quantity + Number(0.1);
				} else {
					quantity = quantity + 1;
				}
				if(quantity > stockAmount){
					ream.getComponent('layout').plugins.modal('show', {
						content: 'Bu Ã¼rÃ¼n den en fazla '+ stockAmount+' '+ stockTypeLanguage +' sepete ekleyebilirsiniz',
						cancelButton: false,
						closeButton: false,
						buttons: {
							primary: {
								title: 'Tamam',
								classes: 'button button-danger',
								callback: function(){
									ream.getComponent('layout').plugins.modal('hide');
								}
							}
						}
					});
					return;
				}
				$('.panel-right .panel-layout').append(ream.getComponent('globalComponent').panel.template.loading);
				ream.getComponent('globalComponent').changeAmount(event, element, cartItemId, quantity);
				element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').html(quantity.toFixed(1))
			} else {
				ream.getComponent('layout').plugins.modal('show', {
					content: 'Bu Ã¼rÃ¼n den en fazla '+ stockAmount+' '+ stockTypeLanguage +' sepete ekleyebilirsiniz',
					cancelButton: false,
					closeButton: false,
					buttons: {
						primary: {
							title: 'Tamam',
							classes: 'button button-danger',
							callback: function(){
								ream.getComponent('layout').plugins.modal('hide');
							}
						}
					}
				});
			}
		},

		cartSelectQuantitiy: function(event, element) {
			var quantity = Number(element.val()),
				cartItemId = element.parents('.cart-quantity-wrap').find('[data-action=cart-quantity]').attr('data-cart-cartItemId');
			$('.panel-right .panel-layout').append(ream.getComponent('globalComponent').panel.template.loading);
			ream.getComponent('globalComponent').changeAmount(event, element, cartItemId, quantity);
		},

		allowOnlyNumericQuantity: function(event, element) {
			var stockType = element.attr('data-cart-stocktype'),
				stockAmount = element.attr('data-cart-stockamount'),
				cartItemId = element.attr('data-cart-cartItemId');
			if(this.decimalStockTypes.indexOf(stockType) > -1){
				if (parseFloat(element.val()) > parseFloat(stockAmount)) {
					element.val(parseFloat(stockAmount));
					return false
				} else {
					if (element.val() != '' && element.val() != 'NaN') {
						if (parseFloat(element.val()) >= 0) {
							if (parseFloat(element.val()) != 0 && parseFloat(element.val()) < 0.5) {
								element.val('0.5');
								return false
							}
							if ((((element.val()) * 10) % 5) != 0) {
								if (isNaN(Math.round(element.val()))) {
									element.val('1');
									return false
								} else {
									element.val(Math.round(element.val()));
								}
							}
						} else {
							element.val('1');
						}
					}
				}

			} else if (this.decimalStockTypes.indexOf(stockType) == -1) {
				if (event.type == 'keyup') {
					if (parseFloat(element.val()) > parseFloat(stockAmount)) {
						element.val(parseFloat(stockAmount));
						return false
					}
				}
			}
		},

		renderCascadeCategories: function(){
			var createCategoryList = function(banners){
				banners = banners['error'] ? {} : banners['banners'];
				$.when(ream.getModels('category').getCascadeList()).done(function(response){
					ream.templateLoader.get({
						compileStorage: true,
						compileStorageId: 'categoryPanel',
						path: 'default/panel/categories',
						rootPath: themePath + '/templates/',
						data: {
							banners: banners,
							categories: response['categories']
						},
						callback: function(response){
							ream.templateLoader.render(defaultSelectors.panel.left, 'html', response, function(){
								ream.getComponent('header').isRenderedPanels.categories = true;
								ream.getComponent('header').renderedAllPanelHTML.categories = response;
							});
						}
					});
				});
			}
			$.when(ream.get('model', 'banner')).done(function(ModelBanner){
				$.when(ModelBanner.getList()).done(function(response){
					createCategoryList(response);
				}).fail(function(response){
					createCategoryList(response);
				});
			});
		},

		selectedCategoryIdList: [],
		categoryIdList: [],
		showCascadeListLevel: function(event, element){
			var id = element.attr('data-id');
			if(ream.getComponent('globalComponent').categoryIdList.indexOf(id) == -1){
				element.html('<div class="button-loading"></div>').addClass('cascade-list-loading');
				$.when(ream.getModels('category').getCascadeList(id)).done(function(response){
					element.parent('li').append(globalTemplateHelpers.helpers.addCascadeSubCategoryList(response)).addClass('sub active');
					ream.getComponent('globalComponent').categoryIdList.push(id);
					ream.getComponent('globalComponent').selectedCategoryIdList[id]= true;
					$('.panel-categories-accordion .open-level-button').removeClass('cascade-list-loading');
					$('.panel-categories-accordion .open-level-button .button-loading').remove();
				});
			} else {
				if(ream.getComponent('globalComponent').selectedCategoryIdList[id]){
					element.next('ul').css('display', 'none');
					$('[data-id="'+ id +'"]').next('ul').css('display','none');
					$('[data-id="'+ id +'"]').next('ul').find('ul').css('display','none');
					$('[data-id="'+ id +'"]').parent('li').removeClass('active sub');
					$('[data-id="'+ id +'"]').next('ul').find('li').removeClass('active sub');
					ream.getComponent('globalComponent').selectedCategoryIdList[id]= false;
					$.each($('[data-id="'+ id +'"]').next('ul').find('span'), function(i, element){
						if(id != i){
							ream.getComponent('globalComponent').selectedCategoryIdList[$(element).attr('data-id')]= false;
						}
					});
				} else {
					element.parent('li').addClass('active sub');
					element.next('ul').css('display', 'block');
					ream.getComponent('globalComponent').selectedCategoryIdList[id]= true;
				}
			}
		},

		plugins: {
			zoom: function(element){
				$(element).each(function () {
					new RTP.PinchZoom($(this));
				});
			}
		},

		eventListeners: function(){
			ream.events.addListener('onRouteBefore', function() {
				ream.getComponent('globalComponent').selectedCategoryIdList = [];
				ream.getComponent('globalComponent').categoryIdList = [];
			});
		}
	}
});

var Helpers = (function(){

	'use strict';

	var defaultSelectors = {
		wrapper: '#wrapper',
		radio: {
			item: '.radio',
			list: '.radio-list'
		},
		checkbox: {
			item: '.checkbox',
			list: '.checkbox-list'
		},
		input: {
			focus: 'input:focus'
		},
		allPhoneInputs: 'input[name=phone], input[name=s_phone], input[name=cell], input[name=s_cell]',
		classes: {
			validate: {
				error: 'validate-error',
				success: 'validate-success'
			}
		}
	};

	var jQWindow = $(window),
		jQDocument = $(document);

	var globalTemplateHelpers,
		Utils;

	if (typeof window !== 'undefined' && window !== null) {
		window.globalTemplateHelpers = globalTemplateHelpers = {};
	}

	globalTemplateHelpers.helpers = {};

	globalTemplateHelpers.addHelper = function(name, helper, argTypes) {
		if (argTypes == null) {
			argTypes = [];
		}
		if (!(argTypes instanceof Array)) {
			argTypes = [argTypes];
		}
		return globalTemplateHelpers.helpers[name] = function() {
			var arg, args, resultArgs, _i, _len;
			Utils.verify(name, arguments, argTypes);
			args = Array.prototype.slice.apply(arguments);
			resultArgs = [];
			for (_i = 0, _len = args.length; _i < _len; _i++) {
				arg = args[_i];
				if (!Utils.isHandlebarsSpecific(arg)) {
					arg = Utils.result(arg);
				}
				resultArgs.push(arg);
			}
			return helper.apply(this, resultArgs);
		};
	};

	globalTemplateHelpers.registerHelpers = function(localHandlebars) {
		var helper, name, _ref, _results;
		if (localHandlebars) {
			globalTemplateHelpers.Handlebars = localHandlebars;
		} else {
			if (typeof window !== 'undefined' && window !== null) {
				globalTemplateHelpers.Handlebars = window.Handlebars;
			}
		}
		globalTemplateHelpers.registerHelper = function(name, helper) {
			return globalTemplateHelpers.Handlebars.registerHelper(name, helper);
		};
		_ref = globalTemplateHelpers.helpers;
		_results = [];
		for (name in _ref) {
			helper = _ref[name];
			_results.push(globalTemplateHelpers.registerHelper(name, helper));
		}
		return _results;
	};

	Utils = {};

	Utils.isHandlebarsSpecific = function(value) {
		return (value && (value.fn != null)) || (value && (value.hash != null));
	};

	Utils.isUndefined = function(value) {
		return (value === void 0 || value === null) || Utils.isHandlebarsSpecific(value);
	};

	Utils.safeString = function(str) {
		return new globalTemplateHelpers.Handlebars.SafeString(str);
	};

	Utils.trim = function(str) {
		var trim;
		trim = /\S/.test("\xA0") ? /^[\s\xA0]+|[\s\xA0]+$/g : /^\s+|\s+$/g;
		return str.toString().replace(trim, '');
	};

	Utils.isFunc = function(value) {
		return typeof value === 'function';
	};

	Utils.isString = function(value) {
		return typeof value === 'string';
	};

	Utils.result = function(value) {
		if (Utils.isFunc(value)) {
			return value();
		} else {
			return value;
		}
	};

	Utils.err = function(msg) {
		throw new Error(msg);
	};

	Utils.verify = function(name, fnArg, argTypes) {
		var arg, i, msg, _i, _len, _results;
		if (argTypes == null) {
			argTypes = [];
		}
		fnArg = Array.prototype.slice.apply(fnArg).slice(0, argTypes.length);
		_results = [];
		for (i = _i = 0, _len = fnArg.length; _i < _len; i = ++_i) {
			arg = fnArg[i];
			msg = '{{' + name + '}} requires ' + argTypes.length + ' arguments ' + argTypes.join(', ') + '.';
			if (argTypes[i].indexOf('safe:') > -1) {
				if (Utils.isHandlebarsSpecific(arg)) {
					_results.push(Utils.err(msg));
				} else {
					_results.push(void 0);
				}
			} else {
				if (Utils.isUndefined(arg)) {
					_results.push(Utils.err(msg));
				} else {
					_results.push(void 0);
				}
			}
		}
		return _results;
	};

	globalTemplateHelpers.addHelper('lowercase', function(str) {
		return str.toLowerCase();
	}, 'string');

	globalTemplateHelpers.addHelper('uppercase', function(str) {
		return str.toUpperCase();
	}, 'string');

	globalTemplateHelpers.addHelper('capitalizeFirst', function(str) {
		return str.charAt(0).toUpperCase() + str.slice(1);
	}, 'string');

	globalTemplateHelpers.addHelper('capitalizeEach', function(str) {
		return str.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1);
		});
	}, 'string');

	globalTemplateHelpers.addHelper('titleize', function(str) {
		var capitalize, title, word, words;
		title = str.replace(/[ \-_]+/g, ' ');
		words = title.match(/\w+/g) || [];
		capitalize = function(word) {
			return word.charAt(0).toUpperCase() + word.slice(1);
		};
		return ((function() {
			var _i, _len, _results;
			_results = [];
			for (_i = 0, _len = words.length; _i < _len; _i++) {
				word = words[_i];
				_results.push(capitalize(word));
			}
			return _results;
		})()).join(' ');
	}, 'string');

	globalTemplateHelpers.addHelper('sentence', function(str) {
		return str.replace(/((?:\S[^\.\?\!]*)[\.\?\!]*)/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}, 'string');

	globalTemplateHelpers.addHelper('reverse', function(str) {
		return str.split('').reverse().join('');
	}, 'string');

	globalTemplateHelpers.addHelper('truncate', function(str, length, omission) {
		if (Utils.isUndefined(omission)) {
			omission = '';
		}
		if (str.length > length) {
			return str.substring(0, length - omission.length) + omission;
		} else {
			return str;
		}
	}, ['string', 'number']);

	globalTemplateHelpers.addHelper('center', function(str, spaces) {
		var i, space;
		spaces = Utils.result(spaces);
		space = '';
		i = 0;
		while (i < spaces) {
			space += '&nbsp;';
			i++;
		}
		return "" + space + str + space;
	}, 'string');

	globalTemplateHelpers.addHelper('newLineToBr', function(str) {
		return str.replace(/\r?\n|\r/g, '<br>');
	}, 'string');

	globalTemplateHelpers.addHelper('sanitize', function(str, replaceWith) {
		if (Utils.isUndefined(replaceWith)) {
			replaceWith = '-';
		}
		return str.replace(/[^a-z0-9]/gi, replaceWith);
	}, 'string');

	globalTemplateHelpers.addHelper('first', function(array, count) {
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		if (Utils.isUndefined(count)) {
			return array[0];
		} else {
			return array.slice(0, count);
		}
	}, 'array');

	globalTemplateHelpers.addHelper('withFirst', function(array, count, options) {
		var item, result;
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		if (Utils.isUndefined(count)) {
			options = count;
			return options.fn(array[0]);
		} else {
			array = array.slice(0, count);
			result = '';
			for (item in array) {
				result += options.fn(array[item]);
			}
			return result;
		}
	}, 'array');

	globalTemplateHelpers.addHelper('last', function(array, count) {
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		if (Utils.isUndefined(count)) {
			return array[array.length - 1];
		} else {
			return array.slice(-count);
		}
	}, 'array');

	globalTemplateHelpers.addHelper('withLast', function(array, count, options) {
		var item, result;
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		if (Utils.isUndefined(count)) {
			options = count;
			return options.fn(array[array.length - 1]);
		} else {
			array = array.slice(-count);
			result = '';
			for (item in array) {
				result += options.fn(array[item]);
			}
			return result;
		}
	}, 'array');

	globalTemplateHelpers.addHelper('after', function(array, count) {
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		return array.slice(count);
	}, ['array', 'number']);

	globalTemplateHelpers.addHelper('withAfter', function(array, count, options) {
		var item, result;
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		array = array.slice(count);
		result = '';
		for (item in array) {
			result += options.fn(array[item]);
		}
		return result;
	}, ['array', 'number']);

	globalTemplateHelpers.addHelper('before', function(array, count) {
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		return array.slice(0, -count);
	}, ['array', 'number']);

	globalTemplateHelpers.addHelper('withBefore', function(array, count, options) {
		var item, result;
		if (!Utils.isUndefined(count)) {
			count = parseFloat(count);
		}
		array = array.slice(0, -count);
		result = '';
		for (item in array) {
			result += options.fn(array[item]);
		}
		return result;
	}, ['array', 'number']);

	globalTemplateHelpers.addHelper('join', function(array, separator) {
		return array.join(Utils.isUndefined(separator) ? ' ' : separator);
	}, 'array');

	globalTemplateHelpers.addHelper('sort', function(array, field) {
		if (Utils.isUndefined(field)) {
			return array.sort();
		} else {
			return array.sort(function(a, b) {
				return a[field] > b[field];
			});
		}
	}, 'array');

	globalTemplateHelpers.addHelper('withSort', function(array, field, options) {
		var item, result, _i, _len;
		result = '';
		if (Utils.isUndefined(field)) {
			options = field;
			array = array.sort();
			for (_i = 0, _len = array.length; _i < _len; _i++) {
				item = array[_i];
				result += options.fn(item);
			}
		} else {
			array = array.sort(function(a, b) {
				return a[field] > b[field];
			});
			for (item in array) {
				result += options.fn(array[item]);
			}
		}
		return result;
	}, 'array');

	globalTemplateHelpers.addHelper('length', function(array) {
		return array.length;
	}, 'array');

	globalTemplateHelpers.addHelper('lengthEqual', function(array, length, options) {
		if (!Utils.isUndefined(length)) {
			length = parseFloat(length);
		}
		if (array.length === length) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['array', 'number']);

	globalTemplateHelpers.addHelper('empty', function(array, options) {
		if (!array || array.length <= 0) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, 'safe:array');

	globalTemplateHelpers.addHelper('isFullText', function(text, options) {
		if (text.length > 0) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, 'safe:string');

	globalTemplateHelpers.addHelper('isFullObject', function(value, options) {
		var name;
		for (name in value) {
			return options.fn(this);
		}
		return options.inverse(this);
	}, 'safe:array');

	globalTemplateHelpers.addHelper('any', function(array, options) {
		if (array && array.length > 0) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, 'safe:array');

	globalTemplateHelpers.addHelper('eachIndex', function(array, options) {
		var index, result, value, _i, _len;
		result = '';
		for (index = _i = 0, _len = array.length; _i < _len; index = ++_i) {
			value = array[index];
			result += options.fn({
				item: value,
				index: index
			});
		}
		return result;
	}, 'array');

	globalTemplateHelpers.addHelper('eachProperty', function(obj, options) {
		var key, result, value;
		result = '';
		for (key in obj) {
			value = obj[key];
			result += options.fn({
				key: key,
				value: value
			});
		}
		return result;
	}, 'object');

	globalTemplateHelpers.addHelper('add', function(value, addition) {
		value = parseFloat(value);
		addition = parseFloat(addition);
		return value + addition;
	}, ['number', 'number']);

	globalTemplateHelpers.addHelper('subtract', function(value, substraction) {
		value = parseFloat(value);
		substraction = parseFloat(substraction);
		return value - substraction;
	}, ['number', 'number']);

	globalTemplateHelpers.addHelper('divide', function(value, divisor) {
		value = parseFloat(value);
		divisor = parseFloat(divisor);
		return value / divisor;
	}, ['number', 'number']);

	globalTemplateHelpers.addHelper('multiply', function(value, multiplier) {
		value = parseFloat(value);
		multiplier = parseFloat(multiplier);
		return value * multiplier;
	}, ['number', 'number']);

	globalTemplateHelpers.addHelper('floor', function(value) {
		value = parseFloat(value);
		return Math.floor(value);
	}, 'number');

	globalTemplateHelpers.addHelper('ceil', function(value) {
		value = parseFloat(value);
		return Math.ceil(value);
	}, 'number');

	globalTemplateHelpers.addHelper('round', function(value) {
		value = parseFloat(value);
		return Math.round(value);
	}, 'number');

	globalTemplateHelpers.addHelper('toFixed', function(number, digits) {
		number = parseFloat(number);
		digits = Utils.isUndefined(digits) ? 0 : digits;
		return number.toFixed(digits);
	}, 'number');

	globalTemplateHelpers.addHelper('toPrecision', function(number, precision) {
		number = parseFloat(number);
		precision = Utils.isUndefined(precision) ? 1 : precision;
		return number.toPrecision(precision);
	}, 'number');

	globalTemplateHelpers.addHelper('toExponential', function(number, fractions) {
		number = parseFloat(number);
		fractions = Utils.isUndefined(fractions) ? 0 : fractions;
		return number.toExponential(fractions);
	}, 'number');

	globalTemplateHelpers.addHelper('toInt', function(number) {
		return parseInt(number, 10);
	}, 'number');

	globalTemplateHelpers.addHelper('toFloat', function(number) {
		return parseFloat(number);
	}, 'number');

	globalTemplateHelpers.addHelper('digitGrouping', function(number, separator) {
		number = parseFloat(number);
		separator = Utils.isUndefined(separator) ? ',' : separator;
		return number.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + separator);
	}, 'number');

	globalTemplateHelpers.addHelper('is', function(value, test, options) {
		if (value && value === test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('isnt', function(value, test, options) {
		if (!value || value !== test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('gt', function(value, test, options) {
		if (value > test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('gte', function(value, test, options) {
		if (value >= test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('lt', function(value, test, options) {
		if (value < test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('lte', function(value, test, options) {
		if (value <= test) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('or', function(testA, testB, options) {
		if (testA || testB) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('and', function(testA, testB, options) {
		if (testA && testB) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	}, ['safe:string|number', 'safe:string|number']);

	globalTemplateHelpers.addHelper('inflect', function(count, singular, plural, include) {
		var word;
		count = parseFloat(count);
		word = count > 1 || count === 0 ? plural : singular;
		if (Utils.isUndefined(include) || include === false) {
			return word;
		} else {
			return "" + count + " " + word;
		}
	}, ['number', 'string', 'string']);

	globalTemplateHelpers.addHelper('log', function(value) {
		return console.log(value);
	}, 'string|number|boolean|array|object');

	globalTemplateHelpers.addHelper('default', function(value, defaultValue) {
		return value || defaultValue;
	}, 'safe:string|number', 'string|number');

	/* External Helpers */
	globalTemplateHelpers.addHelper('ifCompare', function(left, operator, right, options) {
		var bool = false;
		switch (operator) {
			case '==':
				bool = left == right;
				break;
			case '!=':
				bool = left != right;
				break;
			case '>':
				bool = left > right;
				break;
			case '>=':
				bool = left >= right;
				break;
			case '<':
				bool = left < right;
				break;
			case '<=':
				bool = left <= right;
				break;
			default:
				throw 'Unknown operator ' + operator;
		}
		if (bool) {
			return options.fn(this);
		} else {
			return options.inverse(this);
		}
	});

	globalTemplateHelpers.addHelper('url', function(key) {
		return Utils.safeString(window.location[key]);
	}, 'string');

	globalTemplateHelpers.addHelper('unLess', function(context) {
		var args = arguments;
		var options = args[args.length - 1];
		var fn = options.fn,
			inverse = options.inverse;
		options.fn = inverse;
		options.inverse = fn;
		return Handlebars.helpers['if'].apply(this, args);
	});

	globalTemplateHelpers.addHelper('onEven', function(n, block) {
		if ((n % 2) == 0) {
			return block.fn($.extend({}, {
				index: n
			}));
		}
	});

	globalTemplateHelpers.addHelper('onOdd', function(n, block) {
		if ((n % 2)) {
			return block.fn($.extend({}, {
				index: n
			}));
		}
	});

	globalTemplateHelpers.addHelper('encode', function(str) {
		return encodeURIComponent(str);
	}, 'string');

	globalTemplateHelpers.addHelper('htmlText', function(text) {
		var y = document.createElement('textarea');
		y.innerHTML = text;
		return y.value;
	}, 'string');

	globalTemplateHelpers.addHelper('cleanarg', function(input) {
		var y = document.createElement('textarea');
		y.innerHTML = input;
		return y.value.replace(/'/g, "`").replace(/"/g, "``");
	});

	globalTemplateHelpers.addHelper('decodeEntities', function(input) {
		var y = document.createElement('textarea');
		y.innerHTML = input;
		return y.value;
	});

	globalTemplateHelpers.addHelper('groupedEach', function(every, context, options) {
		var output = '',
			subContext = [],
			i;
		if (context && context.length > 0) {
			for (i = 0; i < context.length; i++) {
				if (i > 0 && i % every === 0) {
					output += options.fn(subContext);
					subContext = [];
				}
				subContext.push(context[i]);
			}
			output += options.fn(subContext);
		}
		return output;
	});

	globalTemplateHelpers.addHelper('limit', function(items, limit) {
		/* {{#each (limit products 5)}} */
		if(!ream.utils.isArray(items)){
			return [];
		}
		return items.slice(0, limit);
	});

	globalTemplateHelpers.addHelper('raw', function(value) {
		return Utils.safeString(value);
	}, 'string');

	globalTemplateHelpers.addHelper('pagination', function(currentPage, totalPage, size, locationPrefix, isOrderBy, orderBy, options) {
		var startPage,
			endPage,
			context;
		totalPage = Math.round(totalPage);
		startPage = currentPage - Math.floor(size / 2);
		endPage = currentPage + Math.floor(size / 2);
		if (startPage <= 0) {
			endPage -= (startPage - 1);
			startPage = 1;
		}
		if (endPage > totalPage) {
			endPage = totalPage;
			if (endPage - size + 1 > 0) {
				startPage = endPage - size + 1;
			} else {
				startPage = 1;
			}
		}
		orderBy = orderBy ? orderBy.replace('=', '!') : '';
		context = {
			prevPage: 'paged=' + (parseInt(currentPage) - 1),
			startFromFirstPage: false,
			pages: [],
			locationPrefix: locationPrefix,
			isOrderBy: isOrderBy,
			orderBy: '&orderBy=' + orderBy,
			nextPage: 'paged=' + (parseInt(currentPage) + 1),
			endAtLastPage: false,
		};
		if (startPage === 1) {
			context.startFromFirstPage = true;
		}
		for (var i = startPage; i <= endPage; i++) {
			context.pages.push({
				page: i,
				paged: 'paged=' + i,
				isCurrent: i === currentPage,
			});
		}
		if (endPage === totalPage) {
			context.endAtLastPage = true;
		}
		if(totalPage <= 1){
			return
		}
		return options.fn(context);
	});

	globalTemplateHelpers.addHelper('select', function(items, selected) {
		var output = '';
		var inverted = items.reverse();
		if(ream.utils.isDefined(selected)){
			for(var i = 0; i < inverted.length; i++) {
				var selectedItem;
				for(var j = 0; j < selected.length; j++) {
					if(inverted[i]['id'] == selected[j]['id']){
						selectedItem = inverted[i]['id'];
						output += '<option value="' + inverted[i]['id'] + '" selected="selected">' + inverted[i]['title'] + '</option>';
					}
				}
				if(inverted[i]['id'] != selectedItem){
					output += '<option value="' + inverted[i]['id'] + '">' + inverted[i]['title'] + '</option>';
				}
			}
		}else{
			for(var i = 0; i < inverted.length; i++) {
				output += '<option value="' + inverted[i]['id'] + '">' + inverted[i]['title'] + '</option>';
			}
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('noImagePath', function() {
		return Utils.safeString('/images/no-image.png');
	}, 'string');

	globalTemplateHelpers.addHelper('changePaymentType', function(item) {
		var output = '';
		if(item == 'paypal'){
			output = 'paypalExpressCheckout';
		}else if(item == 'custom'){
			output = 'custompay';
		}else if(item == 'mailorder'){
			output = 'mailOrder';
		}else if(item == 'garantipay'){
			output = 'garantiPay';
		}else if(item == 'bkm'){
			output = 'bkmExpress';
		}else{
			output = item;
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('changePaymentSubmitButtonText', function(item) {
		var output = '';
		if(item == 'garantipay'){
			output = 'Ã–deme Yap';
		}else if(item == 'bkm'){
			output = 'Ã–deme Yap';
		}else{
			output = 'SipariÅŸi Tamamla';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('analyticsRunJs', function(code, name) {
		var analyticsScriptTag = $('<div />').html(code).find('script'),
			analyticsImageTag = $('<div />').html(code).find('img'),
			analyticsIframeTag = $('<div />').html(code).find('iframe');
		analyticsScriptTag.each(function(){
			if($(this).length > 0){
				try{
					eval($(this).html());
				}catch(error){
					var errMsgSuffix = 'alanina eklenen "JavaScript" kodlarinda hata var! Hata Mesaji: ' + error.message;
					if(name == 'cart'){
						console.error('"Sepet Takip Kodu" ' + errMsgSuffix);
					}else if(name == 'product'){
						console.error('"Urun Takip Kodu" ' + errMsgSuffix);
					}else if(name == 'order'){
						console.error('"Siparis Takip Kodu" ' + errMsgSuffix);
					}else if(name == 'product'){
						console.error('"Siparis(Ucuzu) Takip Kodu" ' + errMsgSuffix);
					} else if(name == 'homePage'){
						console.error('"Anasayfa Takip Kodu" ' + errMsgSuffix);
					}
				}
			}
		});
		analyticsImageTag.each(function(){
			if($(this).length > 0){
				$('<img src="' + $(this).attr('src') + '" />');
			}
		});
		analyticsIframeTag.each(function(){
			if($(this).length > 0){
				$('<iframe src="' + $(this).attr('src') + '" />');
			}
		});
	});


	globalTemplateHelpers.addHelper('unMaskPhoneNumber', function(value) {
		if(Utils.isUndefined(value)){
			return
		}
		return Utils.safeString(value.replace('+9', '').replace(/[^\d]/g, ''));
	});

	globalTemplateHelpers.addHelper('noResult', function(object) {
		var output = '';
		if(object.isSearch){
			output = ' \
				<div class="row"> \
					<div class="column"> \
						<div class="no-result-text"> \
							<strong>"' + object.searchQuery + '"</strong> ile ilgili sonuÃ§ bulunamadÄ±. \
						</div> \
					</div> \
				</div> \
				<div class="row"> \
					<div class="column"> \
						<a href="javascript:void(0);" data-action="showTopPanel" data-panel="top" class="button button-soft-gray">Yeniden ara</a> \
					</div> \
				</div>';
		}else if(object.isCategory){
			output = ' \
				<div class="row"> \
					<div class="column"> \
						<div class="no-result-text"> \
							<strong>"' + object.categoryName + '"</strong> kategorisine baÄŸlÄ± Ã¼rÃ¼n bulunamadÄ±. \
						</div> \
					</div> \
				</div> \
				<div class="row"> \
					<div class="column"> \
						<a href="javascript:void(0);" data-action="showCategoriesPanel" data-panel="left" class="button button-soft-gray">DiÄŸer kategoriler iÃ§in tÄ±klayÄ±n</a> \
					</div> \
				</div>';
		}else if(object.isBrand){
			output = ' \
			<div class="row"> \
				<div class="column"> \
					<div class="no-result-text">GÃ¶rÃ¼ntÃ¼lemeye Ã§alÄ±ÅŸtÄ±ÄŸÄ±nÄ±z markaya baÄŸlÄ± Ã¼rÃ¼n bulunamadÄ±.</div> \
				</div> \
			</div> \
			<div class="row"> \
				<div class="column"> \
					<a href="/markalar.html" data-router class="button button-soft-gray">DiÄŸer markalar iÃ§in tÄ±klayÄ±n</a> \
				</div> \
			</div>';
		}else{
			output = ' \
			<div class="row"> \
				<div class="column"> \
					<div class="no-result-text">ÃœrÃ¼n bulunamadÄ±.</div> \
				</div> \
			</div> \
			<div class="row"> \
				<div class="column"> \
					<a href="/" data-router class="button button-soft-gray">Anasayfaya dÃ¶n</a> \
				</div> \
			</div>';
		}
		return Utils.safeString(output);
	}, 'string');

	var convertStringToPrice = function(value){
		return parseFloat(value.replace(/\./g, '').replace(/\,/g, '.'));
	};

	var gainProperty = function(product){
		var property = {
			price: convertStringToPrice((product['price_with_tax'] ? product['price_with_tax'] : product['tax_price_with_calculated_currency'])),
			rebatePrice: convertStringToPrice((product['rebate_price_with_tax'] ? product['rebate_price_with_tax'] : product['tax_rebate_price_with_calculated_currency'])),
			isRebateProduct: product['is_rebate_product'],
			isGain: false
		};
		if(property['isRebateProduct']){
			if(property['price'] > property['rebatePrice']){
				property['isGain'] = true;
			}
		}
		return property;
	}

	globalTemplateHelpers.addHelper('isGain', function(product, options) {
		if(gainProperty(product)['isGain']){
			return options.fn(this);
		}
		return options.inverse(this);
	});

	globalTemplateHelpers.addHelper('gain', function(product) {
		var output;
		if(gainProperty(product)['isGain']){
			output = ((gainProperty(product)['price'] - gainProperty(product)['rebatePrice']).toFixed(2).toString());
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addProductButtons', function(item) {
		var output = '',
			itemArr = [],
			buttons = item['product_buttons'];
		for(var i in buttons){
			itemArr.push(parseInt(buttons[i]));
		}
		if(itemArr.indexOf(1) > -1){
			output += '<div class="row product-buttons">';
		}
		ream.utils.forEach(buttons, function(key, val){
			if(val != '0'){
				output += ' \
				<div class="small-6 columns"> \
					<img class="margin-auto display-block" src="' + buttonPath + key + '.png?revision=' + currentRevision + '"> \
				</div>';
			}
		});
		if(itemArr.indexOf(1) > -1){
			output += '</div>';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addNewBadge', function(isNew) {
		var output = '';
		if(isNew){
			output += '<div class="showcase-badge-new">YENÄ°</div>';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addGiftBadge', function(isGift) {
		var output = '';
		if(isGift){
			output += '<div class="showcase-badge-gift">HEDÄ°YELÄ°</div>';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addRebateBadge', function(isRebate, type, percent, label, usePercent) {
		var output = '',
			rebateProcess = function(){
				if(percent == '0'){
					output += '<div class="showcase-badge-rebate">Ä°NDÄ°RÄ°MLÄ°</div>';
				}else{
					if(label == 'long'){
						output += '<div class="showcase-badge-rebate">% ' + percent + ' Ä°NDÄ°RÄ°MLÄ°</div>';
					}else if(label == 'short'){
						output += '<div class="showcase-badge-rebate">% ' + percent + '</div>';
					}
				}
			}
		if(isRebate){
			if(type != '0'){
				rebateProcess();
			}else{
				if(ream.utils.isDefined(usePercent) && usePercent == true) {
					rebateProcess();
				}else{
					output += '<div class="showcase-badge-rebate">Ä°NDÄ°RÄ°MLÄ°</div>';
				}
			}
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addNoStockBadge', function(isNoStock) {
		var output = '';
		if(isNoStock){
			output += '<span class="showcase-badge-soldout">TÃœKENDÄ°</span>';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addProductPrices', function(item) {
		var output = '';
		if(item['use_price_system']){
			output += '<div class="small-6 columns product-price-column text-right">';
			if(item['is_rebate_product']){
				output += '<div class="product-price-one text-decoration-line-through">' + item['tax_price_with_calculated_currency'] + ' ' + item['system_currency'] + '</div>';
				output += '<div class="product-price-two">' + item['tax_rebate_price_with_calculated_currency'] + ' ' + item['system_currency'] + '</div>';
			}else{
				output += '<div class="product-price font-weight-bold">' + item['tax_price_with_calculated_currency'] + ' ' + item['system_currency'] + '</div>';
			}
			output += '</div>';
		}
		return Utils.safeString(output);
	}, 'string');



	

	globalTemplateHelpers.addHelper('addCartButtons', function(item) {
		var output = '';
		if(item['is_active_add_to_cart_button']){
			output += '<a id="cartProductButton" href="javascript:void(0);" class="button button-large button-block button-primary" data-action="addToCart">Sepete Ekle</a>';
		}else if(item['is_active_no_stock_info']){
			if(item['stock_amount'] > 0){
				output += '<a id="cartProductButton" href="javascript:void(0);" class="button button-large button-block button-danger" data-action="nostock" rel="disable-stock-alert">Stokta Yok</a>';
			}else{
				output += '<a id="cartProductButton" href="javascript:void(0);" class="button button-large button-block button-danger" data-action="nostock">Gelince Haber Ver</a>';
			}
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addVariants', function(item) {
		var output = '';
		if(item['has_variant']){
			if(item['optioned_product_view_type'] == 'dropdown'){
				output+= '\
					<div class="row variant-row"> \
						<div class="column small-12 variant-row-title"> \
							<div class="row-label">SeÃ§enekler</div> \
						</div> \
						<div class="column small-12 variant-row-content">';
							for(var i in item['variants']){
								var a=i;
								output += ' \
									<div class="form-group"> \
										<div class="selectbox selectbox-block"> \
											<select data-selector="selectDropDownVariant" '+ (item['root_product_id'] > 0 ? 'data-selected=true': 'data-selected=false') +' data-group-id="'+ item['variants'][a]['option_group_id'] +'" data-group-index="'+ a +'"> \
												<option value="0">'+ item['variants'][i].option_group_title +' SeÃ§iniz</option>';
												for(var i in item['variants'][a]['options']){
													var optionData = item['variants'][a]['options'][i];
													var disabled = false;
													if((item['root_product_id'] == '0' && a > 0) || optionData['available'] == false){
														disabled = true;
													}
													output += '<option '+ (optionData['selected'] ? 'selected="selected"': '')+'  data-product-id="' + (item['root_product_id'] > 0 ? item['root_product_id'] : item['id']) + '" data-option-id="' + optionData['option_id'] + '" '+ (disabled == true ? 'disabled="disabled"': '')+'" >'+  optionData['option_title'] +' '+ (optionData['has_stock_amount'] == false && optionData['available'] == true ? '( TÃœKENDÄ° )': '')+'</option>';
												}
								output += '</select>'
							output += '</div>';
						output += '</div>';
							}
					output+= '</div>';
				output+= '</div>';
			} else if(item['optioned_product_view_type'] == 'radiobox'){
                output+= '\
					<div class="row variant-row"> \
						<div class="column small-12 variant-row-title"> \
							<div class="row-label">SeÃ§enekler</div> \
						</div> \
						<div class="column small-12 variant-row-content">';
                        for(var i in item['variants']){
                            var a=i;
                            output += ' \
                                            <div class="form-group variant-plural-section" data-selector="selectPluralVariant" '+ (item['root_product_id'] > 0 ? 'data-selected=true': 'data-selected=false') +' data-group-id="'+ item['variants'][a]['option_group_id'] +'" data-group-index="'+ a +'"> ';
                            for(var i in item['variants'][a]['options']){
                                var optionData = item['variants'][a]['options'][i];
                                var disabled = false;
                                if((item['root_product_id'] == '0' && a > 0) || optionData['available'] == false){
                                    disabled = true;
                                }
                                var classNames = ''
								if(optionData['selected']){
									classNames = 'active';
								}
								if(optionData['option_logo'] != ''){
									if(classNames == ''){
										classNames += 'has_logo';
									}else{
										classNames += ' has_logo';
									}
								}
                                output += '<a href="javascript:void(0);" class="'+ classNames +''+ (optionData['has_stock_amount'] == false && optionData['available'] == true ? ' no-stock': '')+'"  data-product-id="' + (item['root_product_id'] > 0 ? item['root_product_id'] : item['id']) + '" data-option-id="' + optionData['option_id'] + '" '+ (disabled == true ? 'disabled="disabled"': '')+'>'+ ( optionData['option_logo'] != '' ? '<img src="'+ optionData['option_logo'] +'">' :  optionData['option_title'] ) +'</a>';
                            }
                            output += '</div>';
                        }
                    output+= '</div>';
                output+= '</div>';
			} else if(item['optioned_product_view_type'] == 'radiobox_multiple'){
				output+= '\
					<div class="row variant-row"> \
						<div class="column small-12 variant-row-title"> \
							<div class="row-label">SeÃ§enekler</div> \
						</div> \
						<div class="column small-12 variant-row-content">';
							for(var i in item['variants']){
								var optionData = item['variants'][i];
								output += ' \
									<div class="radio"> \
										<label class="radio-custom"> \
											<input type="radio" name="singularVariant" data-selector="selectSingularVariant" '+ (optionData.selected ? 'checked="checked"' : '') +' data-slug="'+ optionData.product_slug+'"/> \
											<span></span> \
											' + item['variants'][i].label + ' \
											' + (item['variants'][i].stock_amount == "0" ? '( TÃœKENDÄ° )': '') + ' \
										</label> \
									</div> ';
							}
					output+= '</div>';
				output+= '</div>';
			}
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('addSelections', function(item) {
		var output = '';
		for(var i in item['selections']){
			output += ' \
			<div class="row product-selection-row"> \
				<div class="small-12 columns"> \
					<div class="row-label" data-selection-title="' + item['selections'][i]['label'] + '">' + item['selections'][i]['label'] + '</div> \
				</div> \
				<div class="small-12 columns"> \
					<div class="selectbox medium selectbox-block"> \
						<select class="form-control" data-action="selectAdditionalFeatures" data-selection-title="' + item['selections'][i]['label'] + '"> \
						<option value="0">SeÃ§iniz</option>';
			for(var j in item['selections'][i]['options']){
				output += '<option value="' + item['selections'][i]['options'][j]['id'] + '">' + item['selections'][i]['options'][j]['label'] + '</option>';
			}
			output += '</select></div></div></div>';
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('selectMaxImage', function(image, type) {
		var output = image;
		if(ream.utils.isDefined(type) && type == 'tablet'){
			if(ream.utils.isTablet()){
				output = image.replace(/_min\./g, '.');
			}
		}else{
			output = image.replace(/_min\./g, '.');
		}
		return Utils.safeString(output);
	}, 'string');

	globalTemplateHelpers.addHelper('convertToSlug', function(slug, id, type) {
		var charMap = {
		    from: function() {
		        return ["Ğ°", "Ğ±", "Ğ²", "Ğ³", "Ğ´", "Ğµ", "Ğ¶", "Ğ·", "Ğ¸", "Ğ¹", "Ğº", "Ğ»", "Ğ¼", "Ğ½", "Ğ¾", "Ğ¿", "Ñ€", "Ñ", "Ñ‚", "Ñƒ", "Ñ„", "Ñ…", "Ñ†", "Ñ‡", "Ñˆ", "Ñ‰", "ÑŠ", "ÑŒ", "Ñ", "Ñ", "Ğ", "Ğ‘", "Ğ’", "Ğ“", "Ğ”", "Ğ•", "Ğ–", "Ğ—", "Ğ˜", "Ğ™", "Ğš", "Ğ›", "Ğœ", "Ğ", "Ğ", "ĞŸ", "Ğ ", "Ğ¡", "Ğ¢", "Ğ£", "Ğ¤", "Ğ¥", "Ğ¦", "Ğ§", "Ğ¨", "Ğ©", "Ğª", "Ğ¬", "Ğ®", "Ğ¯", "Ğ‡", "Ñ—", "Ğ„", "Ñ”", "Ğ«", "Ñ‹", "Ğ", "Ñ‘", "Ä±", "Ä°", "ÄŸ", "Ä", "Ã¼", "Ãœ", "ÅŸ", "Å", "Ã¶", "Ã–", "Ã§", "Ã‡", "Ã", "Ã¡", "Ã‚", "Ã¢", "Ãƒ", "Ã£", "Ã€", "Ã ", "Ã‡", "Ã§", "Ã‰", "Ã©", "ÃŠ", "Ãª", "Ã", "Ã­", "Ã“", "Ã³", "Ã”", "Ã´", "Ã•", "Ãµ", "Ãš", "Ãº"]
		    },
		    to: function() {
		        return ["a", "b", "v", "g", "d", "e", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sht", "a", "y", "yu", "ya", "A", "B", "B", "G", "D", "E", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "Ts", "Ch", "Sh", "Sht", "A", "Y", "Yu", "Ya", "I", "i", "Ye", "ye", "I", "i", "Yo", "yo", "i", "I", "g", "G", "u", "U", "s", "S", "o", "O", "c", "C", "A", "a", "A", "a", "A", "a", "A", "a", "C", "c", "E", "e", "E", "e", "I", "i", "O", "o", "O", "o", "O", "o", "U", "u"]
		    }
		};
		slug = slug.toString();
		slug = ream.utils.strReplace(charMap.from(), charMap.to(), slug);
		slug = slug.replace(/[^\w ]+/g,'').replace(/ +/g,'-');
		switch (type) {
			case 'product':
				slug = '/' + slug + ',PR-' + id + '.html';
				break;
			case 'category':
				slug = '/' + slug + ',LA_' + id + '-2.html';
				break;
			case 'brand':
				slug = '/' + slug + ',LA_' + id + '-3.html';
				break;
			case 'page':
				slug = '/' + slug + ',DP-' + id + '.html';
				break;
			case 'news':
				slug = '/' + slug + ',AR-' + id + '.html';
				break;
		}
		return Utils.safeString(slug);
	}, 'string');

	globalTemplateHelpers.addHelper('addBanner', function(items, id) {
		for(var i in items){
			if(items[i].id == id){
				var output = '';
				if(ream.utils.isObject(items[i]) && ream.utils.isDefined(items[i])){
					if(ream.utils.isEmpty(items[i].link)){
						output += '<img src="/myassets/banner_pictures/' + items[i].picture + '?rev=' + items[i].revision + '" alt="" />';
					}else{
						if(items[i].alt == 'no-router'){
							output += '<a href="' + items[i].link + '"' + (items[i].target ? 'target="_blank"' : '') + '><img src="/myassets/banner_pictures/' + items[i].picture + '?rev=' + items[i].revision + '" alt="" /></a>';
						}else{
							output += '<a href="' + items[i].link + '" data-router><img src="/myassets/banner_pictures/' + items[i].picture + '?rev=' + items[i].revision + '" alt="" /></a>';
						}
					}
				}else{
					console.error('Banner Items is invalid');
				}
				return Utils.safeString(output);
			}
		}
	}, 'string');

	globalTemplateHelpers.addHelper('addSlider', function(namespace, options, id) {
		if(ream.utils.isDefined(options)){
			if(ream.utils.has(options, 'parentItems')){
				for(var i in options.parentItems){
					if(options.parentItems[i].id == id){
						var currentItems = options.parentItems[i].items;
						var output = '<div id="' + namespace + '" class="swiper-container"><div class="swiper-wrapper">';
						if(ream.utils.isObject(currentItems) && ream.utils.isDefined(currentItems)){
							for(var j in currentItems) {
								if(!currentItems[j].target){
									output += ' \
									<div class="swiper-slide"> \
										<a href="' + currentItems[j].link + '" data-router> \
											<img src="/myassets/slider_pictures/' + currentItems[j].image + '?rev=' + options.parentItems[i].revision + '" alt="" /> \
										</a> \
									</div>';
								}else{
									output += ' \
									<div class="swiper-slide"> \
										<a href="' + currentItems[j].link + '" target="_blank"> \
											<img src="/myassets/slider_pictures/' + currentItems[j].image + '?rev=' + options.parentItems[i].revision + '" alt="" /> \
										</a> \
									</div>';
								}
							}
						}else{
							console.error('Slider Items is invalid');
						}
						output += '</div>';
						if(options.pagination){
							output += '<div class="' + namespace + '-pagination swiper-pagination"></div>';
						}
						output += '</div>';
						return Utils.safeString(output);
					}
				}
			}
		}
	}, 'string');

	var categoryHTMLOutput;
	var recursiveCategories = function(type, data, hasImage){
		if(type == 'accordion'){
			for (var i in data) {
				var parentLabel = data[i].label;
				var subHTMLOutput = (!ream.utils.isEmptyObject(data[i].children) ? '<li class="sub">' : '<li>');
				categoryHTMLOutput += subHTMLOutput + '<a href="' + data[i].link + '" data-router>' + data[i].label + '</a>';
				if (!ream.utils.isEmptyObject(data[i].children)) {
					categoryHTMLOutput += '<span class="open-level-button" data-action="showListLevelOpen"></span>';
					categoryHTMLOutput += '<ul>';
					if(type != 'accordion') {
						categoryHTMLOutput += ' \
						<li class="list-level-title"> \
							<div class="back-to-level" data-action="showListLevelClose">' + parentLabel + '</div> \
						</li>';
					}
					recursiveCategories(type, data[i].children, hasImage);
					categoryHTMLOutput += '</ul>'
				}
				categoryHTMLOutput += '</li>';
			}
		} else if(type == 'cascade') {
			for (var i in data) {
				categoryHTMLOutput += '\
				<li '+ (hasImage == 'img' ? 'class="image-list"': ' ') +'> \
					<a href="'+ data[i].link +'" data-router>'+ (hasImage == 'img' ? '<img src="'+ data[i].logo_path +'"/>' : ' ') +' '+ data[i].label +'</a> \
					'+ (data[i].has_children == '1' ? '<span class="open-level-button" data-id="'+ data[i].id +'" data-action="showCascadeListLevel"></span>' : '') +' \
				</li>';
			}
		}
		return categoryHTMLOutput;
	};

	globalTemplateHelpers.addHelper('addCascadeSubCategoryList', function(items) {
		var data = items.categories;
		var output = '<ul style="display: block;">';
			for (var i in data){
				output += ' \
					<li> \
						<a href="'+ data[i].link +'" data-router>'+ data[i].label +'</a> \
						'+ (data[i].has_children == '1' ? '<span class="open-level-button" data-id="'+ data[i].id +'" data-action="showCascadeListLevel"></span>' : '') +' \
					</li>';
			}
		output += '</ul>';
		return output;
	});

	globalTemplateHelpers.addHelper('addCategoryList', function(type, items, hasImage) {
		categoryHTMLOutput = '';
		categoryHTMLOutput += recursiveCategories(type, items, hasImage);
		return Utils.safeString(categoryHTMLOutput);
	}, 'string');

	var XHRNotice = function(message, buttonText, isRedirect){
		ream.getComponent('layout').notice(message, function(){
			ream.getComponent('layout').plugins.modal('hide');
			ream.utils.wait(function(){
				if(isRedirect){
					ream.router.navigate('/');
				}else{
					ream.router.reload();
				}
			}, 50);
		}, buttonText);
	};

	var XHRNoticeMessage = {
		'theme': {
			'failedload': 'Tema dosyasÄ± load edilemedi.'
		},
		'500': 'Belirlenemeyen bir hata oluÅŸtu. <br /> LÃ¼tfen site yÃ¶neticinizle iletiÅŸime geÃ§iniz.',
		'timeout': 'Åu anda talebinizi yerine getirmekte sorun yaÅŸÄ±yoruz.',
		'503': 'Åu anda talebinizi yerine getirmekte sorun yaÅŸÄ±yoruz.',
		'noconnection': 'MaÄŸazaya baÄŸlanÄ±lamÄ±yor. AÄŸ baÄŸlantÄ±nÄ±zÄ±n etkin olduÄŸundan emin olun ve yeniden deneyin.'
	};

	var XHRNoticeButtonTexts = ['Tamam', 'Tekrar Deneyin'];

	var XHRFailRedirectDisallowedURLs = [
		'/store_mobile/slider/list',
		'/store_mobile/slider/view/id/',
		'/store_mobile/banner/list',
		'/store_mobile/banner/view/id/',
		'/store_mobile/member/logout',
		'/store_mobile/member/forgotPass',
		'/store_mobile/member/activate',
		'/store_mobile/default/addMaillist',
		'/store_mobile/default/sendContactMail',
		'/store_mobile/default/securityCode',
		'/store_mobile/default/desktopSite',
		'/store_mobile/cart/add',
		'/store_mobile/cart/delete/itemId/',
		'/store_mobile/city/listBy/countryId/',
		'/store_mobile/shippingCompany/listBy/cityId/',
		'/store_mobile/comment/add/productId/',
		'/store_mobile/product/comment/list/productId/',
		'/store_mobile/product/similarProducts/id/',
		'/store_mobile/product/offeredProducts/id/'
	];

	var ajaxProcessing = [];

	var ajaxPrefilter = function(){
		$.ajaxPrefilter(function (options, originalOptions, XHR) {
			ajaxProcessing.push(options);
			if(!ream.utils.has(originalOptions, 'type')){
				options.type = 'POST';
			}
			if(!ream.utils.has(originalOptions, 'async')){
				options.async = false;
			}
			if(!ream.utils.has(originalOptions, 'dataType')){
				options.dataType = 'json';
			}
			if(!ream.utils.has(originalOptions, 'timeout')){
				options.timeout = 30000;
			}
			var originalSuccess = options.success,
				originalBeforeSend = options.beforeSend;
			if (options.cache) {
				var cacheUrl = ream.ajax.localCache.cacheKey(originalOptions);
				options.beforeSend = function () {
					originalBeforeSend();
				}
				options.success = function (response) {
					if (!ream.ajax.localCache.exist(cacheUrl)) {
						ream.ajax.localCache.set(cacheUrl, response);
					}
					originalSuccess();
				}
			}
			options.complete = function(){
				ajaxProcessing.splice(-1, 1);
				if(ream.utils.isEmpty(ajaxProcessing)){
					ream.events.emit('allAjaxRequestComplete');
				}
			}
			XHR.fail(function(XHR, textStatus, errorThrown) {
				var message = options.url + ': ';
				if(XHR.status == 404){
					var urlLastId = parseInt(options.url.substr(options.url.lastIndexOf('/') + 1));
					if(ream.utils.isNumber(urlLastId)){
						options.url = options.url.replace(new RegExp(urlLastId), '');
					}
					if(XHRFailRedirectDisallowedURLs.indexOf(options.url) == -1){
						if(options.url.indexOf('.tpl') > -1){
							XHRNotice(XHRNoticeMessage['theme']['failedload'], XHRNoticeButtonTexts[0], true);
						}else{
							if(ream.getCurrentControllerByStateName() != 'notFound'){
								ream.events.addListener('ajaxMakerError', function(){
									ream.bootstrap('ream.controllers.default', 'notFound');
									this.removeEvent('ajaxMakerError');
								});
							}
						}
					}
					message += 'Requested page not found.';
				}else if (XHR.status == 500){
					XHRNotice(XHRNoticeMessage['500'], XHRNoticeButtonTexts[0], true);
					message += 'Internal Server Error';
				}else if (errorThrown == 'timeout') {
					message += 'Request time out.';
					XHRNotice(XHRNoticeMessage['timeout'], XHRNoticeButtonTexts[1]);
				} else if (errorThrown == 'abort') {
					message += 'Request was aborted.';
				} else if (XHR.status === 503) {
					message += 'Service Unavailable.';
					XHRNotice(XHRNoticeMessage['503'], XHRNoticeButtonTexts[1]);
				} else if (XHR.status === 0) {
					message += 'No connection.';
					/* XHRNotice(XHRNoticeMessage['noconnection'], XHRNoticeButtonTexts[1]); */
				} else {
					message += 'Unknown error.';
				}
				console.error(message);
			});
		});
	}

	var ajaxTransport = function(){
		$.ajaxTransport("+*", function (options, originalOptions, XHR) {
			if(!ream.utils.has(options, 'cache')){
				return
			}
			var cacheUrl = ream.ajax.localCache.cacheKey(originalOptions);
			if (ream.ajax.localCache.exist(cacheUrl)) {
				return {
					send: function (headers, completeCallback) {
						ream.utils.wait(function(){
							var response = {};
							response['data'] = ream.ajax.localCache.get(cacheUrl);
							completeCallback(200, 'OK', response);
						}, 0);
					}
				};
			}
		});
	}

	var setGlobalValidateSettings = function(){
		$.validator.setDefaults({
			errorElement: 'div',
			errorClass: defaultSelectors.classes.validate.error,
			validClass: defaultSelectors.classes.validate.success,
			onsubmit: false,
			onkeyup: function (element, event) {
				if (event.which === 9 && this.elementValue(element) === '') {
					return
				} else if (element.name in this.submitted || element === this.lastElement) {
					this.element(element);
				}
			},
			success: function(element){
				element.remove();
			},
			errorPlacement: function (error, element) {
				if (element.parents(defaultSelectors.radio.list).size() > 0) {
					error.appendTo(element.parents(defaultSelectors.radio.item));
				} else if (element.parents(defaultSelectors.checkbox.list).size() > 0) {
					error.appendTo(element.parents(defaultSelectors.checkbox.item));
				} else {
					error.insertAfter(element);
				}
			},
			invalidHandler: function(form, validator) {
				if (!validator.numberOfInvalids()){
					return
				}
				if($(form.delegateTarget).data('errorScrolltop')){
					var wScrollTop = $(defaultSelectors.wrapper).scrollTop(),
						eOffsetTop = $(validator.errorList[0].element).offset().top,
						scrollValue = ((wScrollTop + eOffsetTop) - 65);
					ream.utils.wait(function(){
						$(defaultSelectors.wrapper).scrollTop(scrollValue);
						$(defaultSelectors.input.focus).trigger('blur');
					}, 100);
				}
			}
		});
	}

	var checkIdNumber = function(value, element){
		var idNo = value;
		var oneRule = (Number(idNo[0]) + Number(idNo[1]) + Number(idNo[2]) + Number(idNo[3]) + Number(idNo[4]) + Number(idNo[5]) + Number(idNo[6]) + Number(idNo[7]) + Number(idNo[8]) + Number(idNo[9])) % 10;
        var strOneResult = String(oneRule);

        var twoRule = (((Number(idNo[0]) + Number(idNo[2]) + Number(idNo[4]) + Number(idNo[6]) + Number(idNo[8])) * 7) - (Number(idNo[1]) + Number(idNo[3]) + Number(idNo[5]) + Number(idNo[7]))) % 10;
        var strTwoResult = String(twoRule);

		if(idNo.length != 0) {
			if(idNo[0] == '0' || strOneResult !== idNo[10] || strTwoResult !== idNo[9]) {
				return false;
			}else{
				return true;
			}
		}
		return true;
	}

	var checkCreditCardNumber = function (value) {
		var cardNumber = value.length;
        if (cardNumber < 16 || cardNumber > 16) {
            return false;
        }
        return true;
    }

	var addGlobalValidateMethods = function(){
		$.validator.addMethod('specialNumber', function(value, element) {
			return this.optional(element) || value === 'NA' || value.match(/^[0-9,\+-_ ]+$/);
		}, 'Please enter a valid number, or "NA"');
		$.validator.addMethod('tcNo', function(value, element) {
			return checkIdNumber(value, element);
		}, 'LÃ¼tfen geÃ§erli bir tc kimlik numarasÄ± giriniz.');
		$.validator.addMethod('email', function(value, element){
			return this.optional(element) || /(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*")@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$/i.test(value);
		}, 'Verify you have a valid email address.');
		$.validator.addMethod('creditCardNumber', function(value, element) {
			return checkCreditCardNumber(value, element);
		});
	}

	var googleAnalyticsSendPageView = function(url, title){
		if(typeof ga != 'undefined'){
			ga('set', {
			  page: '/' + url,
			  title: title
			});
			ga('send', 'pageview');
			if(typeof dataLayer != 'undefined'){
				tagManagerSendPageView(url);
			}
		} else if(typeof dataLayer != 'undefined'){
			tagManagerSendPageView(url);
		}
		ream.events.emit('onGoogleAnalyticsSendPageView', url, title);
	}

	var tagManagerSendPageView = function (url) {
		$(dataLayer).each(function( index,value ) {
			if(value[0] == 'config'){
				dataLayer.splice(index, 1);
				gtag('config', value[1] , {'page_path': '/' + url});
			}
		});
	}

	var googleAnalyticsDocumentLoadedCount = 0;

	var googleAnalyticsUrlHandler = function(){
		var getSendingUrl = function(currentFragment){
			var sendingUrl = decodeURIComponent(currentFragment);
			return sendingUrl;
		}
		ream.events.addListener('onDocumentLoaded', function(){
			if(googleAnalyticsDocumentLoadedCount != 0){
				googleAnalyticsSendPageView(getSendingUrl(ream.router.getPath()), document.title);
			}
			googleAnalyticsDocumentLoadedCount++;
		});
	}

	var setGlobalEventListeners = function(){
		jQDocument.on({
			keydown: function(event){
			    if ($(this).val().length > 11) {
			        $(this).val($(this).val().substr(0, 11));
					return false;
			    }
			},
			keyup: function(event) {
				var key = event.which || event.keyCode;
				if ($.inArray(key, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (key == 65 && ( event.ctrlKey === true || event.metaKey === true ) ) || (key >= 35 && key <= 40)) {
					return
				}
				if ((event.shiftKey || (key < 48 || key > 57)) && (key < 96 || key > 105)) {
					return false;
				}
				if (key === 32){
					return false;
				}
			},
			change: function() {
				this.value = this.value.replace(/\s/g, '');
			}
		}, defaultSelectors.allPhoneInputs);

		var routeFn = function(event, isDevice){
			var target = isDevice == 'mobile' ? $(event.currentTarget) : $(event.target);
			if(isDevice == 'desktop'){
				if(event.which != 1){
					return
				}
			}
			var href = target.attr('href') || target.parents('a').attr('href');
			var data = ream.utils.isDefined(target.attr('data-router')) || ream.utils.isDefined(target.parents('a').attr('data-router'));
			if(data){
				if(ream.router.getPath() != ream.router.clearSlashes(href).replace(/^.*\/\/[^\/]+/, '')){
					ream.router.navigate(href);
				}else{
					ream.events.emit('onEqualUrl');
				}
				event.preventDefault();
				return false
			}
			return true
		}

		jQDocument.on('click', '[data-router]', function(event){
			routeFn.call(this, event, ream.utils.isMobile() ? 'mobile' : 'desktop');
		});
	}

	var setPrivateForceRouteUrls = function(){
		if(ream.utils.isUndefined(isPrivateShopping) && ream.utils.isUndefined(isMemberLogin)){
			return
		}
		if(isPrivateShopping && !isMemberLogin){
			ream.utils.extend(ream.router.force, {
				url: '/uye-girisi',
				status: true,
				allowPatterns: [
					/uye-girisi/,
					/uye-ol/,
					/sifremi-unuttum/,
					/sifremi-unuttum\?p=(.+)/,
					/index.php\?do=member\/login/,
					/index.php\?do=member\/login?next=order2/,
					/index.php\?do=member\/b2blogin/,
					/index.php\?do=member\/signup/,
					/index.php\?do=member\/activate\&id=([\d]+)\&acode=(.+)/,
					/index.php\?do=member\/forgotpass$/,
					/index.php\?do=member\/forgotpass\&p=(.+)/
				]
			});
		}
	}

	var AutoReloadDisallowedURLs = [
		'index.php?do=catalog/orderFinished',
		'index.php?do=catalog/orderError'
	];

	var autoReloadPage = function() {
	    var hidden, visibilityChange;
	    if (typeof document.hidden !== 'undefined') {
	        hidden = 'hidden';
	        visibilityChange = 'visibilitychange';
	    } else if (typeof document.mozHidden !== 'undefined') {
	        hidden = 'mozHidden';
	        visibilityChange = 'mozvisibilitychange';
	    } else if (typeof document.webkitHidden !== 'undefined') {
	        hidden = 'webkitHidden';
	        visibilityChange = 'webkitvisibilitychange';
	    }
	    function handleVisibilityChange() {
	        if (!document[hidden] && ream.initialized) {
				if(AutoReloadDisallowedURLs.indexOf(ream.router.getPath()) == -1){
		            // ream.reload();
				}
	        }
	    }
	    if (typeof document.addEventListener === 'undefined' || typeof document[hidden] === 'undefined') {
	        console.error('This demo requires a modern browser that supports the Page Visibility API.');
	    } else {
	        document.addEventListener(visibilityChange, handleVisibilityChange, false);
	    }
	}

	return {
		initialize: function(){
			globalTemplateHelpers.registerHelpers(Handlebars);
			ajaxPrefilter();
			ajaxTransport();
			googleAnalyticsUrlHandler();
			addGlobalValidateMethods();
			setGlobalValidateSettings();
			setGlobalEventListeners();
			setPrivateForceRouteUrls();
			autoReloadPage();
		},
		convertToSlug: function(slug, id, type){
			return globalTemplateHelpers.helpers.convertToSlug(slug, id, type)['string'];
		},
		controlFloatOnly: function(e) {
			if (Helpers.controlDecimalOnly(e)) {
				return true;
			}
			var keynum;
			if (window.event) {
				keynum = e.keyCode;
			} else if (e.which) {
				keynum = e.which;
			}
			if (Number(keynum) == 191) {
				return true;
			}
			return false;
		},
		controlDecimalOnly: function(e){
			var keynum;
			var keychar;
			var numcheck;
			if(window.event){
				keynum = e.keyCode;
			}else if(e.which){
				keynum = e.which;
			}
			if((Number(keynum)<=123 && Number(keynum)>=112) || Number(keynum)==13 || Number(keynum)==9 || Number(keynum)==8){
				return true;
			}
			if((Number(keynum)>=48 && Number(keynum)<=57) || Number(keynum)==191 || (Number(keynum)>=96 && Number(keynum)<=105)){
				return true;
			}
			return false;
		}
	}

}());

/* Initialize Helpers */
Helpers.initialize();
