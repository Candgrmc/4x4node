/* Layout */
ream.addComponent('layout', function() {

	'use strict';

	var defaultSelectors = {
		body: 'body',
		loading: '.loading',
		wrapper: '#wrapper',
		header: '#header',
		footer: '#footer',
		panel: {
			overlay: '.overlay',
			input: '.panel input',
			left: '.panel-left',
			right: '.panel-right',
			top: '.panel-top'
		},
		showcase: {
			div: '.showcase',
			image: '.showcase-image'
		},
		scrollTop: '.scroll-top'
	}

	var jQWindow = $(window),
		jQDocument = $(document);

	return {

		initialize: function(){
			ream.getComponent('layout').eventListeners();
		},

		events: {
			'tap [data-action=showListLevelClose]': 'levelBack',
			'tap [data-action=showListLevelOpen]': 'showListLevel',
			'tapstart [data-action=closeOverlay]': 'closePanelTabStart',
			'tapstart [data-action=closePanel]': 'closePanelTabStart',
			'keyup input[name=securityCode]': 'autoUpperCase',
			'tapstart .scroll-top': 'scrollTop'
		},

		panel: {
			effect: {
				timeout: 300
			},
			template: {
				loading: '<div class="loading dark-loading"><div class="loading-inner"><div class="circle"></div></div></div>'
			}
		},

		closePanelTabStart: function(event){
			ream.getComponent('layout').isContinueSearch = true;
			ream.getComponent('layout').closePanel();
		},

		overlay: function(type, hue){
			if(type == 'show'){
				if(hue == 'dark'){
					$(defaultSelectors.panel.overlay).addClass('overlay-dark').fadeIn(0);
					return this
				}
				$(defaultSelectors.panel.overlay).removeClass('overlay-dark').fadeIn(0);
			}else if(type == 'hide'){
				$(defaultSelectors.panel.overlay).fadeOut(0);
			}
			return this
		},

		isPanelActive: false,

		openPanel: function(position, isLoading, callback){
			if(isLoading){
				$('.panel-' + position + ' > .panel-layout').html(ream.getComponent('layout').panel.template.loading);
			}
			ream.getComponent('layout').plugins.modal('hide');
			ream.getComponent('layout').onBlurActiveInput().overlay('show', (position == 'top' ? 'dark' : 'transparent'));
			$(defaultSelectors.body).addClass('with-panel-' + position + '-cover').addClass('with-panel-' + position + '-visible');
			setTimeout(function(){
				if (callback && ream.utils.isFunction(callback)) {
					callback();
				}
			}, ream.getComponent('layout').panel.effect.timeout);
			ream.getComponent('layout').isPanelActive = true;
			return this
		},

		closePanel: function(){
			if(ream.getComponent('layout').isPanelActive){
				var closeAction = function(){
					ream.getComponent('layout').clearPanelWithClass();
					setTimeout(function(){
						ream.getComponent('layout').overlay('hide').clearPanelWithClass().clearPanelWithVisibleClass();
					}, ream.getComponent('layout').panel.effect.timeout);
					ream.getComponent('layout').onBlurActiveInput().isPanelActive = false;
					ream.getComponent('layout').isContinueSearch = false;
					ream.events.emit('onClosePanel');
				}
				closeAction();
			}
			return this
		},

		clearPanelWithClass: function(){
			$(defaultSelectors.body).removeClass('with-panel-top-cover').removeClass('with-panel-left-cover').removeClass('with-panel-right-cover');
			return this
		},

		clearPanelWithVisibleClass: function(){
			$(defaultSelectors.body).removeClass('with-panel-top-visible').removeClass('with-panel-left-visible').removeClass('with-panel-right-visible');
			return this
		},

		showListLevel: function(event, element){
			if(element.parents('.list').hasClass('panel-categories-accordion')){
				element.next('ul').show();
				element.parent('li').addClass('active');
				element.attr('data-action', 'showListLevelClose');
			}
		},

		levelBack: function(event, element){
			if(element.parents('.list').hasClass('panel-categories-accordion')){
				element.attr('data-action', 'showListLevelOpen');
				element.parent('li').find('.open-level-button').attr('data-action', 'showListLevelOpen');
				element.parent('li').removeClass('active');
				element.parent('li').find('li').removeClass('active');
				element.parent('li').find('ul').hide();
			}
		},

		setHeight: function(type, options, callback){
			switch (type) {
				case 'showcase':
					$('.showcase-image').height($('.showcase-image').width()).promise().done(function(){
						if (callback && ream.utils.isFunction(callback)) {
							callback();
						}
					});
				break;
				case 'custom':
					$(options.target).height($(options.source).height()).promise().done(function(){
						if (callback && ream.utils.isFunction(callback)) {
							callback();
						}
					});
				break;
			}
			return this
		},

		scrollTop: function(value){
			if(ream.utils.isObject(value) || ream.utils.isUndefined(value)){
				value = 0;
			}
			$(defaultSelectors.wrapper).scrollTop(value);
		},

		scrolling: function(type, target){
			var eventListener = function(event){
				event.preventDefault();
			}
			if(type == 'disable'){
				jQDocument.on('mousewheel touchmove', target, eventListener);
			}else if(type == 'enable'){
				jQDocument.off('mousewheel touchmove', target, eventListener);
			}
		},

		autoUpperCase: function(event, element){
			if(/[^0-9A-Z]/.test(element.val())){
				element.val(element.val().toUpperCase().replace(/([^0-9A-Z])/g,""));
			}
		},

		onBlurActiveInput: function(){
			$('input:focus').trigger('blur');
			return this
		},

		notice: function(message, callback, buttonText, useOverlayClose){
			useOverlayClose = ream.utils.isUndefined(useOverlayClose) || !useOverlayClose ? false : true;
			ream.getComponent('layout').plugins.modal('show', {
				content: message,
				overlayClick: useOverlayClose,
				cancelButton: false,
				closeButton: false,
				buttons: {
					primary: {
						title: buttonText ? buttonText : 'Tamam',
						classes: 'button button-danger',
						callback: function(){
							if (callback && ream.utils.isFunction(callback)) {
								callback();
							}else{
								ream.getComponent('layout').plugins.modal('hide');
							}
						}
					}
				}
			});
		},

		eventListeners: function(){

			jQWindow.on('resize', function(){
				ream.getComponent('layout').setHeight('showcase');
			});

			ream.events.addListener('onRouteBefore', function() {
				if(!ream.getComponent('footer').isRendered){
					ream.getComponent('footer').render();
				}
				if(ream.getComponent('layout').isPanelActive){
					ream.getComponent('layout').closePanel();
				}
				ream.getComponent('layout').plugins.modal('hide');
				ream.getComponent('header').cleanFromSearchPanel();
				ream.getComponent('content').startLoading();
			});

			ream.events.addListener('onClosePanel', function(){
				ream.getComponent('header').cleanFromSearchPanel();
			});

			ream.events.addListener('onEqualUrl', function(){
				ream.getComponent('layout').closePanel();
			});

		},

		plugins: {
			modal: function(type, options){
				var Modal = jQWindow.data('plugin_Modal');
				if(type == 'show'){
					if(ream.utils.isDefined(Modal)){
						Modal.destroy();
					}
					if(!ream.utils.has(options, 'width')){
						ream.utils.extend(options, {
							width: ream.utils.isTablet() ? '70%' : '90%'
						});
					}
					jQWindow.Modal(options);
				}else{
					if(ream.utils.isDefined(Modal)){
						Modal.destroy();
					}
				}
			},
			slider: function(element, options){
				new Swiper(element, options);
			}
		},

		setTitle: function(title){
			document.title = siteTitle;
			if(!ream.utils.isEmpty(siteTitle)){
				if(ream.utils.isDefined(title)){
					document.title += ' ~ ' + title;
				}else{
					document.title = siteTitle;
				}
			}else{
				document.title = title;
			}
		}

	}

});

/* Header */
ream.addComponent('header', function() {

	'use strict';

	var defaultSelectors = {
		header: '#header',
		search: '[data-action=search]',
		searchInput: '[data-action=searchInput]',
		searchClose: '.search-close',
		cartProductButton: '#cartProductButton',
		productImages: '.product-images',
		loginForm: '[data-action=loginForm]',
		loginButton: '[data-action=loginButton]',
		applyTokenCodeForm: '[data-action=applyTokenCodeForm]',
		applyTokenCodeButton: '[data-action=applyTokenCodeButton]',
		badge: {
			member: '#header .header-right > .member-processes',
			cart: '#header .header-right > .shopping-cart'
		},
		panel: {
			top: '.panel-top > .panel-layout',
			left: '.panel-left > .panel-layout',
			right: '.panel-right > .panel-layout'
		}
	}

	return {

		initialize: function(){
			ream.getComponent('header').render();
		},

		isRendered: false,

		render: function(){
			ream.templateLoader.get({
				compileStorage: true,
				compileStorageId: 'header',
				path: 'default/header',
				rootPath: themePath + '/templates/',
				data: {
					logoPath: logoPath,
					isLogin: isMemberLogin,
					totalAmount: cartItemAmount
				},
				callback: function(response){
					ream.templateLoader.render(defaultSelectors.header, 'html', response, function(){
						ream.getComponent('header').isRendered = true;
					});
				}
			});
		},

		events: {
			'tapstart [data-action=showTopPanel]' : 'onFocusSearchInput',
			'keyup [data-action=searchInput]' : 'showSearchClose',
			'tapstart [data-action=cleanSearchInput]' : 'cleanSearchInput',
			'tapstart [data-action=showCategoriesPanel]' : 'showCategories',
			'tapstart [data-action=showMemberPanel]' : 'showMember',
			'tapstart [data-action=showCartPanel]' : 'showCart',
			'tap [data-action=showCartPanelTwo]' : 'showCart',
			'tapstart [data-action=removeFromCart]' : 'removeFromCart',
			'submit [data-action=applyTokenCodeForm]' : 'applyTokenCode',
			'submit [data-action=search]' : 'searchResults',
			'tap [data-action=logoutButton]' : 'logoutAction',
			'tap [data-action=loginButton]' : 'loginAction',
			'submit [data-action=loginForm]' : 'loginAction',
			'tap [data-action=getOrderAddress]' : 'getOrderAddress',
			'tap [data-action=showProductOptions]' : 'showProductOptions',
			'tap [data-action=addToCartPromotionProductOptions]' : 'addToCartPromotionProductOptions',
			'tap [data-action=selectPromotion]' : 'selectPromotion'
		},

		isRenderedPanels: {
			search: false,
			categories: false,
			cart: false,
			member: false,
			login: false,
			notlogin: false
		},

		renderedAllPanelHTML: {
			search: '',
			categories: '',
			member: '',
			login: '',
			notlogin: ''
		},

		onFocusSearchInput: function(){
			$(defaultSelectors.searchInput).trigger('focus');
		},

		showSearch: function(){
			if(ream.getComponent('header').isRenderedPanels.search){
				ream.getComponent('layout').openPanel('top', false);
				return
			}
			ream.getComponent('layout').openPanel('top', true, function(){
				ream.getComponent('header').renderSearch();
			});
		},

		showSearchClose: function(event, element){
			if(element.val().length > 0){
				$(defaultSelectors.searchClose).show();
			}else{
				ream.getComponent('header').cleanFromSearchPanel();
			}
		},

		cleanSearchInput: function(event, element){
			ream.getComponent('header').cleanFromSearchPanel();
		},

		renderSearch: function(event, element){
			ream.templateLoader.get({
				compileStorage: true,
				compileStorageId: 'searchPanel',
				path: 'default/panel/search',
				callback: function(response){
					ream.templateLoader.render(defaultSelectors.panel.top, 'html', response, function(){
						ream.getComponent('header').isRenderedPanels.search = true;
						ream.getComponent('header').renderedAllPanelHTML.search = response;
					});
				}
			});
		},

		showCategories: function(){
			if(ream.getComponent('header').isRenderedPanels.categories){
				ream.getComponent('layout').openPanel('left', false);
				return
			}
			ream.getComponent('layout').openPanel('left', true, function(){
				$.when(ream.get('model', 'category')).done(function(){
					ream.getComponent('header').renderCategories();
				});
			});
		},

		renderCategories: function(){
			$.when(ream.getModels('category').getList()).done(function(response){
				ream.templateLoader.get({
					compileStorage: true,
					compileStorageId: 'categoryPanel',
					path: 'default/panel/categories',
					rootPath: themePath + '/templates/',
					data: {
						categories: response['categories']
					},
					callback: function(response){
						ream.templateLoader.render(defaultSelectors.panel.left, 'html', response, function(){
							ream.getComponent('header').isRenderedPanels.categories = true;
							ream.getComponent('header').renderedAllPanelHTML.categories = response;
						});
					}
				});
			});
		},

		reloadCategoryPanel: function(){
			if(ream.getComponent('header').renderedAllPanelHTML.categories != ''){
				$(defaultSelectors.panel.left).html(ream.getComponent('header').renderedAllPanelHTML.categories);
			}
		},

		userType: '',

		showMember: function(withoutMembers, withOrder){
			withOrder = ream.utils.isDefined(withOrder) && !ream.utils.isObject(withOrder) ? withOrder : false;
			if(isMemberLogin){
				ream.getComponent('header').userType = 'member';
			}else{
				ream.getComponent('header').userType = 'login';
			}
			if(withoutMembers === true && ream.utils.isBoolean(withoutMembers)){
				ream.getComponent('header').userType = 'notlogin';
			}else{
				withoutMembers = false;
			}
			if(ream.getComponent('header').isRenderedPanels[ream.getComponent('header').userType]){
				ream.templateLoader.render(defaultSelectors.panel.right, 'html', ream.getComponent('header').renderedAllPanelHTML[ream.getComponent('header').userType], function(){
					ream.getComponent('layout').openPanel('right');
				});
				return
			}
			ream.getComponent('layout').openPanel('right', true, function(){
				$.when(ream.get('model', 'member')).done(function(){
					ream.getComponent('header').renderMember(withoutMembers, withOrder);
				});
			});
		},

		renderMember: function(withoutMembers, withOrder){
			var options = {
				path: 'default/panel/member',
				rootPath: themePath + '/templates/'
			}
			if(isMemberLogin){
				ream.utils.extend(options, {
					data: {
                        memberUsePaymentSystem: memberUsePaymentSystem
                    },
                    path: 'default/panel/myaccount',
					rootPath: themePath + '/templates/'
				});
			}else{
				ream.utils.extend(options, {
					compileStorage: true,
					compileStorageId: 'memberPanel',
					data: {
						withOrder: withOrder,
						withoutMembers: withoutMembers,
						isMemberSignup: isMemberSignup,
						isFacebookLogin: isFacebookLogin,
						facebookURL: facebookURL,
						orderWithoutRegistration: orderWithoutRegistration
					}
				});
			}
			ream.utils.extend(options, {
				callback: function(response){
					ream.templateLoader.render(defaultSelectors.panel.right, 'html', response, function(){
						ream.getComponent('header').isRenderedPanels[ream.getComponent('header').userType] = true;
						ream.getComponent('header').renderedAllPanelHTML[ream.getComponent('header').userType] = response;
					});
				}
			});
			ream.templateLoader.get(options);
		},

		showCart: function(options){
			if(ream.utils.isDefined(options)){
				if(ream.utils.has(options, 'useClean') && options.useClean){
					ream.getComponent('header').cleanCacheFromCart();
					ream.getComponent('header').isRenderedPanels.cart = false;
				}
			}
			if(ream.getComponent('header').isRenderedPanels.cart){
				ream.templateLoader.render(defaultSelectors.panel.right, 'html', ream.getComponent('header').renderedAllPanelHTML.cart, function(){
					ream.getComponent('layout').openPanel('right');
				});
				return
			}
			ream.getComponent('layout').openPanel('right', true, function(){
				$.when(ream.get('model', 'cart'), ream.get('model', 'order')).done(function(){
					ream.getComponent('header').renderCart();
				});
			});
		},

		cartItemAmount: cartItemAmount,

		renderCart: function(element, event){
			var cartRenderAction = function(response){
				ream.templateLoader.render(defaultSelectors.panel.right, 'html', response, function(){
					ream.getComponent('header').isRenderedPanels.cart = true;
					ream.getComponent('header').renderedAllPanelHTML.cart = response;
				});
			}
			if(ream.getComponent('header').cartItemAmount == '0'){
				ream.templateLoader.get({
					path: 'default/panel/cart',
					rootPath: themePath + '/templates/',
					data: {
						error: true
					},
					callback: function(response){
						cartRenderAction(response);
					}
				});
				return
			}
			$.when(ream.getModels('order').getCartProducts()).done(function(response){
				var isCartPromotional = false,
					cartPromotionalProperties = {
						isFullPromotionalProducts: false
					};
				if(typeof usePromotional != 'undefined'){
					if(usePromotional && ream.utils.has(response, 'promotions')){
				        if(!ream.utils.isEmptyObject(response['promotions'])){
				            isCartPromotional = true;
				        }
				    }
				};
				if(isCartPromotional){
					ream.utils.extend(cartPromotionalProperties, {
						discount: response['promotional_discount'],
						totalPriceWithDiscount: response['cart_total_price_with_promotional_discount'],
						isDiscount: (ream.utils.has(response, 'promotional_discount') ? true : false),
						promotions: response['promotions']
					});
					if(ream.utils.has(response, 'promotional_products') && response['promotional_products'].length > 0){
						ream.utils.extend(cartPromotionalProperties, {
							isFullPromotionalProducts: true,
							products: response['promotional_products']
						})
					}
				}
				ream.getComponent('header').addBadge('cart', response['cart_total_amount']);
				ream.templateLoader.get({
					compileStorage: true,
					compileStorageId: 'cartPanel',
					path: 'default/panel/cart',
					rootPath: themePath + '/templates/',
					data: {
						tokenCode: response['token_code'],
						useTokenSystem: response['use_token_system'],
						trackCartCode: response['track_cart_code'],
						cartTotalPrice: response['cart_total_price'],
						cartProducts: response['cart_products'],
						cartTotalAmount: response['cart_total_amount'],
						cartPromotionalProperties: cartPromotionalProperties,
						isCartPromotional: isCartPromotional
					},
					callback: function(response){
						cartRenderAction(response);
					}
				});
			}).fail(function(){
				ream.templateLoader.get({
					path: 'default/panel/cart',
					rootPath: themePath + '/templates/',
					data: {
						error: true
					},
					callback: function(response){
						cartRenderAction(response);
					}
				});
				ream.getComponent('header').addBadge('cart', '0');
			});
		},

		showProductOptions: function(event, element){
			ream.getComponent('layout').openPanel('right', true, function(){
				$.when(ream.get('model', 'product')).done(function(){
					ream.getComponent('header').renderProductOptions(element.attr('data-id'));
				});
			});
		},

		renderProductOptions: function(id){
			var productOptionsRenderAction = function(response){
				ream.templateLoader.render(defaultSelectors.panel.right, 'html', response);
			};
			$.when(ream.getModels('product').getProductOptions(id)).done(function(response){
				ream.templateLoader.get({
					compileStorage: true,
					compileStorageId: 'productOptionsPanel' + id,
					path: 'default/panel/productoptions',
					data: response,
					callback: function(response){
						productOptionsRenderAction(response);
					}
				});
			});
		},

		addToCartPromotionProductOptions: function(event, element){
			ream.get('controller', 'cart').done(function(CartController){
				CartController.actions.add({
					id: element.attr('data-id'),
					quantity: 1,
					button: element,
					onComplete: function(response){
						ream.getComponent('header').showCart({
							useClean: true
						});
					}
				});
			});
		},

		selectPromotion: function(event, element){
			ream.get('model', 'cart').done(function(CartModel){
				$.when(CartModel.selectPromotion({
					id: element.attr('data-id'),
					button: element
				})).done(function(){
					ream.getComponent('header').showCart({
						useClean: true
					});
				});
			});
		},

		cleanBadgeFromCart: function(){
			ream.getComponent('header').isRenderedPanels.cart = false;
			ream.getComponent('header').renderedAllPanelHTML.cart = '';
			return this
		},

		cleanCacheFromCart: function(){
			ream.ajax.localCache.remove('/store_mobile/order/step1-GET');
			return this
		},

		cleanCacheFormCartProduct: function(id){
			ream.ajax.localCache.remove('/store_mobile/product/view/id/' + id + '-GET');
			return this
		},

		getOrderAddress: function(){
			var currentState = ream.getCurrentControllerByStateName();
			var getOrderAddressRedirect = function(callback){
				if(currentState == 'address' || currentState == 'payment'){
					ream.getComponent('layout').closePanel();
				}else{
					if(callback && ream.utils.isFunction(callback)){
						callback();
					}
				}
			};
			if(isMemberLogin){
				getOrderAddressRedirect(function(){
					ream.router.navigate('index.php?do=catalog/order2');
				});
			}else{
				getOrderAddressRedirect(function(){
					ream.getComponent('header').showMember(true, true);
					// ream.router.navigate('index.php?do=member/login&withoutMembers=true');
				});
			}
		},

		removeFromCart: function(event, element){
			ream.getComponent('layout').plugins.modal('show', {
				content: 'ÃœrÃ¼nÃ¼ silmek istediÄŸinize emin misiniz?',
				cancelButton: true,
				buttons: {
					primary: {
						title: 'ÃœrÃ¼nÃ¼ Sil',
						loading: true,
						classes: 'button button-danger',
						callback: function(){
							ream.get('controller', 'cart').done(function(CartController){
								var removedProductId = element.attr('data-item-id');
								CartController.actions.remove({
									id: removedProductId,
									onComplete: function(){
										if(ream.hasCurrentController('product')){
											var productSoldOut = $(defaultSelectors.productImages).find('.showcase-badge-soldout'),
												cartProductButton = $(defaultSelectors.cartProductButton);
											if(removedProductId == (ream.getCurrentControllerByQueryParams('productId') + '_1')){
												if(cartProductButton.attr('data-action') == 'nostock'){
													cartProductButton.attr('data-action', 'addToCart').removeClass('button-danger').addClass('button-primary').html('Sepete Ekle');
												}
												if(productSoldOut.length){
													productSoldOut.remove();
												}
											}
										}
										ream.getComponent('header').showCart();
										ream.getComponent('layout').plugins.modal('hide');
									}
								});
							});
						}
					}
				}
			});
		},

		applyTokenCode: function(event, element){
			if(element.is(':disabled')){
				return
			}
			ream.get('controller', 'abstract').done(function(AbstractController){
				var form = $(defaultSelectors.applyTokenCodeForm);
		        var isFormValidation = AbstractController['defaultActions'].isValid({
		            form: form,
		            model: 'order'
		        });
		        if(isFormValidation){
					AbstractController['defaultActions'].submit('applyTokenCode', 'order', {
						form: defaultSelectors.applyTokenCodeForm,
						button: defaultSelectors.applyTokenCodeButton
					}).done(function(){
						var currentState = ream.getCurrentControllerByStateName();
						ream.getComponent('header').showCart({
							useClean: true
						});
						if(currentState == 'payment'){
							ream.reload();
						}
					}).fail(function(response){
						ream.getComponent('layout').notice(response['responseJSON']['msg']);
					});
		        }
			});
		},

		isContinueSearch: false,

		searchResults: function(event, element){
			if(ream.getComponent('layout').isContinueSearch){
				return
			}
			var value = element.find('input[type=search]').val();
			var url = ream.utils.getSearchUrlPathName();
			if(ream.utils.isEmpty(value)){
				ream.getComponent('layout').closePanel();
				return
			}else if(value.length < 2){
				ream.getComponent('layout').closePanel();
				setTimeout(function(){
					ream.getComponent('layout').notice('GirdiÄŸiniz deÄŸer en az 2 karakter olmalÄ±.', function(){
						ream.getComponent('layout').plugins.modal('hide');
						ream.getComponent('header').cleanFromSearchPanel();
					});
				}, 200);
				return
			}
			ream.getComponent('layout').onBlurActiveInput();
			ream.getComponent('header').cleanFromSearchPanel();
			if(url == value){
				return
			}
			ream.router.navigate('arama/' + value);
		},

		cleanFromSearchPanel: function(){
			var value = $(defaultSelectors.searchInput).val();
			if(ream.utils.isDefined(value) && value.length > 0){
				$(defaultSelectors.searchInput).val('');
			}
			if($(defaultSelectors.searchClose).css('display') == 'block'){
				$(defaultSelectors.searchClose).hide();
			}
		},

		logoutAction: function(){
			ream.getComponent('layout').closePanel();
			ream.getComponent('content').startLoading();
			$.when(ream.getModels('member').logout()).done(function(){
				isMemberLogin = false;
				ream.ajax.localCache.allRemove();
				if(isPrivateShopping){
					ream.router.force.status = true;
					ream.router.navigate('index.php?do=member/login');
				}else{
					var currentState = ream.getCurrentControllerByStateName();
					if(currentState == 'orders' || currentState == 'profile' || currentState == 'bankTransferForm' || currentState == 'ideapayList' || currentState == 'ideapayShow' || currentState == 'shippinginQuiry'){
						ream.router.navigate('/');
					}else{
						ream.reload();
					}
				}
				ream.getComponent('content').stopLoading({
					useScrollTop: false
				});
				ream.getComponent('header').cleanBadgeFromCart().addBadge('cart', '0').removeBadge('user');
			}).fail(function(response){
				ream.getComponent('layout').notice(response['responseJSON']['msg'], function(){
					ream.getComponent('header').showMember();
				});
			});
		},

		loginAction: function(event, element){
			if(element.is(':disabled')){
				return
			}
			ream.get('controller', 'abstract').done(function(AbstractController){
				var form = $(defaultSelectors.loginForm);
		        var isFormValidation = AbstractController['defaultActions'].isValid({
		            form: form,
		            model: 'member'
		        });
		        if(isFormValidation){
					AbstractController['defaultActions'].submit('login', 'member', {
						form: defaultSelectors.loginForm,
					    button: defaultSelectors.loginButton
                    }).done(function(response){
                        memberUsePaymentSystem = response.member_use_payment_system;
						var currentState = ream.getCurrentControllerByStateName(),
							withOrder = $(defaultSelectors.loginForm).data('withOrder');
						isMemberLogin = true;
						if(isPrivateShopping){
							ream.router.force.status = false;
						}
						ream.ajax.localCache.allRemove();
						if(currentState == 'login' || currentState == 'signup' || currentState == 'forgotPassword' || currentState == 'contactUs'){
							ream.router.navigate('/');
							ream.getComponent('layout').closePanel();
						}else{
							if(withOrder){
								ream.getComponent('layout').onBlurActiveInput();
								setTimeout(function(){
									ream.router.navigate('index.php?do=catalog/order2');
								}, 200);
							}else{
								if(currentState == 'activate'){
									ream.router.navigate('/');
									ream.getComponent('header').showMember();
								}else{
									ream.reload();
									ream.getComponent('header').showMember();
								}
							}
						}
						ream.getComponent('header').addBadge('user');
					}).fail(function(response){
						ream.getComponent('layout').notice(response['responseJSON']['msg']);
					});
		        }
			});
		},

		addBadge: function(type, amount){
			if(type == 'user'){
				$(defaultSelectors.badge.member).append('<div class="badge badge-primary"><i class="icon icon-checkmark"></i></div>');
			}else if(type == 'cart'){
				if(parseFloat(amount) == 0){
					$(defaultSelectors.badge.cart).find(' > .badge').addClass('hidden');
				}else{
					$(defaultSelectors.badge.cart).find(' > .badge').removeClass('hidden').html(amount);
				}
			}
			return this
		},

		removeBadge: function(type, amount){
			if(type == 'user'){
				$(defaultSelectors.badge.member).find(' > .badge').remove();
			}else if(type == 'cart'){
				$(defaultSelectors.badge.cart).find(' > .badge').html('0');
			}
			return this
		}

	}

});

/* Content */
ream.addComponent('content', function() {

	'use strict';

	var defaultSelectors = {
		body: 'body',
		wrapper: '#wrapper',
		content: '#content',
		productList: '#product-list',
		loading: '.loading',
		preLoadItems: '.preLoadItems'
	}

	return {

		initialize: function(){
			return
		},

		getLoadingTemplate: function(currentElement){
			var isWrapper = $(currentElement).is(defaultSelectors.wrapper),
				isProductList = $(currentElement).is(defaultSelectors.productList),
				CSSOpts = {
					top: '',
					marginTop: ''
				};
			if(isWrapper){
				ream.utils.extend(CSSOpts, {
					top: '',
					marginTop: ''
				});
			}else if(isProductList){
				var offset = $(defaultSelectors.productList).offset();
				ream.utils.extend(CSSOpts, {
					top: ' style="top: ' + (offset.top - 15) + 'px!important"',
					marginTop: ' style="margin-top: -120px!important"'
				});
			}
			return 	' <div class="loading"> ' +
						' <div class="loading-inner"> ' +
							' <div class="circle"></div> ' +
						' </div> ' +
					' </div> '
		},

		startLoading: function(options){
			if(!options && ream.utils.isUndefined(options)){
				options = {};
			}
			var currentElement = ream.utils.isDefined(options.element) ? options.element : defaultSelectors.wrapper,
				isWrapper = $(currentElement).is(defaultSelectors.wrapper),
				isProductList = $(currentElement).is(defaultSelectors.productList);
			if($(defaultSelectors.loading).length == 0){
				ream.getComponent('layout').scrolling('disable', '.loading');
				$(defaultSelectors.body).addClass('with-loading-cover');
				$(defaultSelectors.body).append(ream.getComponent('content').getLoadingTemplate(currentElement));
			}
		},

		stopLoading: function(options){
			if(!options && ream.utils.isUndefined(options)){
				options = {};
			}
			var currentElement = ream.utils.isDefined(options.element) ? options.element : defaultSelectors.wrapper;
			if($(defaultSelectors.preLoadItems).length > 0){
				$(defaultSelectors.preLoadItems).remove();
			}
			$(defaultSelectors.loading).remove();
			$(defaultSelectors.body).removeClass('with-loading-cover');
			ream.getComponent('layout').scrolling('enable', '.loading');
			if(ream.utils.isUndefined(options.useScrollTop)){
				ream.getComponent('layout').scrollTop(0);
			}else{
				if(options.useScrollTop){
					ream.getComponent('layout').scrollTop(0);
				}
			}
		}

	}

});

/* Footer */
ream.addComponent('footer', function() {

	'use strict';

	var defaultSelectors = {
		footer: '#footer',
		addMaillistForm: '[data-action=addMaillistForm]',
		addMaillistButton: '[data-action=addMaillistButton]'
	}

	return {

		initialize: function(){
			ream.getComponent('footer').render();
		},

		isRendered: false,

		render: function(){
			var getSliderListAndCreateView = function(banners, brands, menu){
				banners = banners['error'] ? {} : banners['banners'];
				brands = brands['error'] ? {} : brands['brands'];
				ream.templateLoader.get({
					compileStorage: true,
					compileStorageId: 'footer',
					path: 'default/footer',
					rootPath: themePath + '/templates/',
					data: {
						banners: banners,
						brands: brands,
						menu: {
							items: menu['menu_items'][2],
							length: ream.utils.objectLength(menu['menu_items'][2])
						}
					},
					callback: function(response){
						ream.templateLoader.render(defaultSelectors.footer, 'html', response, function(){
							ream.getComponent('footer').isRendered = true;
							ream.getComponent('footer').afterRender();
						});
					}
				});
			}
			$.when(ream.get('model' , 'banner')).done(function(){
				$.when(ream.getModels('banner').getList()).done(function(banner){
					$.when(ream.get('model' , 'brand')).done(function(){
						$.when(ream.getModels('brand').getList()).done(function(brand){
							$.when(ream.get('model' , 'menu')).done(function(MenuModel){
								$.when(MenuModel.getList()).done(function(menu){
									getSliderListAndCreateView(banner, brand, menu);
								}).fail(function(menu){
									getSliderListAndCreateView(banner, brand, menu);
								});
							});
						}).fail(function(brand){
								getSliderListAndCreateView(banner, brand);
						});
					});
				}).fail(function(response){
						getSliderListAndCreateView(response);
				});
			});
		},

		afterRender: function() {
			ream.getComponent('layout').plugins.slider('#brands-slider', {
				slidesPerView: 2,
				spaceBetween: 16,
				autoplay: 3000
			});
		},

		events: {
			'tap [data-action=addMaillistButton]' : 'addMaillist',
			'submit [data-action=addMaillistForm]' : 'addMaillist',
			'tap [data-action=desktopSite]' : 'desktopSite'
		},

		addMaillist: function(event, element){
			if(element.is(':disabled')){
				return
			}
			var formElement = $(defaultSelectors.addMaillistForm);
			var value = formElement.find('input[type=email]').val();
			var submitButton = $(defaultSelectors.addMaillistButton);
			var submitButtonRemoveLoading = function(message){
				submitButton.removeAttr('disabled');
				submitButton.removeClass('button-loading');
			}
			if(ream.utils.isEmpty(value)){
				setTimeout(function(){
					ream.getComponent('layout').notice('Email alanÄ± boÅŸ bÄ±rakÄ±lamaz.');
					submitButtonRemoveLoading();
				}, 150);
				return
			}
			$.when(ream.get('model', 'default')).done(function(){
				ream.get('controller', 'abstract').done(function(AbstractController){
					AbstractController['defaultActions'].submit('addMaillist', 'default', {
						form: defaultSelectors.addMaillistForm,
						button: defaultSelectors.addMaillistButton
					}).done(function(response){
						ream.getComponent('layout').notice(response['msg']);
						submitButtonRemoveLoading();
						formElement.find('input[type=email]').val(' ');
					}).fail(function(response){
						ream.getComponent('layout').notice(response['responseJSON']['msg']);
						submitButtonRemoveLoading();
					});
				});
			});
		},

		desktopSite: function() {
            ream.get('model', 'default').done(function(ModelDefault){
                ModelDefault.setDesktopSite();
            });
        }

	}

});
