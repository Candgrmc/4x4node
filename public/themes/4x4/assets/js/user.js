/* jQuery Tab Plugin */
jQuery.fn.tabs = function(options) {
	var options = jQuery.extend({
        ajax: false,
        type:"",
        url:"",
		tabs : this,
		tabHeadClick : '._tabHead li a',
		tabHeadSelectHtmlTag : '._tabHead li',
		tabHeadSelectClass : '_tabHead-selected',
        tabHeadSelectLink : '_tabHead-selected a',
		tabContentSelectHtmlTag : '._tabContent > div',
		tabContentSelectClass : '_tabContent-selected'
	}, options);
	return jQuery(this).each(function(){
         
        var totalTab = jQuery(options.tabs).find(options.tabHeadClick).length;
		jQuery(this).find(options.tabHeadClick).click(function(){
            var clickTab = jQuery(this).parent().index();
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).removeClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).eq(clickTab).addClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).removeClass(options.tabContentSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(clickTab).addClass(options.tabContentSelectClass);
            var selectedElement = jQuery(this);
            if(options.ajax && selectedElement.attr("rel")!='loaded'){
                jQuery.ajax({
                    type:options.type,
                    url:options.url,
                    data:"tabId="+clickTab,
                    success:function(data){
                        jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(clickTab).html(data);
                        selectedElement.attr("rel","loaded");
                    }
                });
            }
		});
        
        /* Tab Next Button */
        jQuery('._tabContentNextButton').click(function(){
            var Eq = jQuery(this).parents(options.tabContentSelectHtmlTag).index();
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).removeClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).eq(parseFloat(Eq+1)).addClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).removeClass(options.tabContentSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(parseFloat(Eq+1)).addClass(options.tabContentSelectClass);
            /* Kontrol Konulabilir
            if(parseFloat(totalTab-1) == parseFloat(Eq+1)){
                //alert('a');
                jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).removeClass(options.tabHeadSelectClass);
                jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).eq(parseFloat(0)).addClass(options.tabHeadSelectClass);
                jQuery(options.tabs).find(options.tabContentSelectHtmlTag).removeClass(options.tabContentSelectClass);
                jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(parseFloat(0)).addClass(options.tabContentSelectClass);
            }else{
                jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).removeClass(options.tabHeadSelectClass);
                jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).eq(parseFloat(Eq+1)).addClass(options.tabHeadSelectClass);
                jQuery(options.tabs).find(options.tabContentSelectHtmlTag).removeClass(options.tabContentSelectClass);
                jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(parseFloat(Eq+1)).addClass(options.tabContentSelectClass);
            }
            */
        });
        
        /* Tab Prev Button */
        jQuery('._tabContentPrevButton').click(function(){
            var Eq = jQuery(this).parents(options.tabContentSelectHtmlTag).index();
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).removeClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabHeadSelectHtmlTag).eq(parseFloat(Eq-1)).addClass(options.tabHeadSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).removeClass(options.tabContentSelectClass);
            jQuery(options.tabs).find(options.tabContentSelectHtmlTag).eq(parseFloat(Eq-1)).addClass(options.tabContentSelectClass);
        });
        
        if(options.ajax){
            jQuery(options.tabs).find("."+options.tabHeadSelectLink).click();
        }
	});
};
 