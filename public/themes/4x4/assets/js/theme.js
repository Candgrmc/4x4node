jQuery(document).ready(function(){
	if(navigator.appName == "Microsoft Internet Explorer") {
		jQuery('[placeholder]').focus(function() {
			var input = jQuery(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function() {
			var input = jQuery(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur().parents('form').submit(function() {
			jQuery(this).find('[placeholder]').each(function() {
				var input = jQuery(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
				}
			})
		});
	}
	
	// Scroll Top
	jQuery(window).scroll(function(){
		if (jQuery(this).scrollTop() > 100) {
			jQuery("#scrollTop").fadeIn();
		} else {
			jQuery("#scrollTop").fadeOut();
		}
	}); 
	jQuery("#scrollTop").click(function(){
		jQuery("html, body").animate({scrollTop:0}, 600);
		return false
	});
	
	// Remove Block
	var uriList = [
		'?do=members/login',
		'?do=static/contactus', 
		'?do=member/signup',
		'?do=members/signup',
		'?do=catalog/order',
		'?do=catalog/order2',
		'?do=catalog/order3',
		'?do=catalog/orderFinished',
		'?do=members/login2',
		'?do=static/contactForm',
		'?do=default/contactForm',
		'?do=static/btForm',
		'?do=member/btForm',
		'?do=members/forgotpass',
		'?do=static/supportForm',
		'?do=dynamic/start',
		'?do=members/funcsuccess',
		'?do=members/shippinginQuiry',
		'?do=members/payments',
		'?do=members/payment',
		'?do=members/paymentDirectionForm'
	];
	var urlLocation = window.location.search;
	if(jQuery.inArray(urlLocation, uriList) > -1){
		jQuery('.leftBlocks').parent().remove();
		jQuery('.contentSection').removeClass().addClass('contentSection-Page');
	}
	
	var news = window.location.pathname;
	if(news == '/haberler.html' || news== '/iletisim' || news == '/iletisim-formu' || news == '/uye-girisi' || news == '/uye-girisi2' || news == '/uye-ol' || news == '/sifremi-unuttum' || news == '/sepet'){
		jQuery('.leftBlocks').parent().remove();
		jQuery('.contentSection').removeClass().addClass('contentSection-Page');
	}
	
	var locationSearch = (window.location.search).split('&');
	if(locationSearch[0] == "?do=catalog/order"){
		jQuery('.leftBlocks').parent().remove();
		jQuery('.contentSection').removeClass().addClass('contentSection-Page');
	}
	
	// Cart Drop Down
	function iCartClickOne(){
		jQuery('.iCartContent').stop(true,true).fadeIn(200);
		jQuery('.iCart > a').addClass('iCartLinkActive');
	}
	
	function iCartClickTwo(){
		jQuery('.iCartContent').fadeOut(200);
		jQuery('.iCart > a').removeClass('iCartLinkActive');
	}
	
	jQuery('.iCart > a').click(function(){
		if (jQuery('.iCartContent').css('display') == 'none'){
			iCartClickOne();
		}else {
			iCartClickTwo();
		}
	});
	
	jQuery(document).mouseup(function(e){
		if(jQuery(e.target).parents('.iCart').length == 0) {
			jQuery('.iCartContent').fadeOut(200);
			jQuery('.iCart > a').removeClass('iCartLinkActive');
		}
	});
	
	// Search Drop Down
	function searchClickOne(){
		jQuery('.searchContent').stop(true,true).fadeIn(200);
		jQuery('.search').addClass('searchActive');
	}
	
	function searchClickTwo(){
		jQuery('.searchContent').fadeOut(200);
		jQuery('.search').removeClass('iCartLinkActive');
	}
	
	jQuery('.search > a').click(function(){
		if (jQuery('.searchContent').css('display') == 'none'){
			searchClickOne();
		}else {
			searchClickTwo();
		}
	});
	
	jQuery(document).mouseup(function(e){
		if(jQuery(e.target).parents('.search').length == 0) {
			jQuery('.searchContent').fadeOut(200);
			jQuery('.search').removeClass('iCartLinkActive');
		}
	});
});
 