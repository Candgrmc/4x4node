function addToCart($this){
    var id = $this.attr('data-id')
    var price = $this.attr('data-price');
    var minimum = $this.attr('data-minimum');
    var img = $this.attr('data-img');
    var name = $this.attr('data-name');
    var slug = $this.attr('data-slug');
    var option = $this.attr('data-option');
    var optioncheck = $this.attr('data-option-check');

    if(optioncheck){
      if(option){
        $.post('/add-to-cart',{id:id,name:name,slug:slug,price:price,minimum:minimum,img:img,option:option},function(res){


                $('#cartBody').append('<tr>\n' +
                    '                                            <td>\n' +
                    '                                                <div class="product-media">\n' +
                    '                                                    <a href="#">  <img src="'+img+'" alt="'+name+'"></a>\n' +
                    '                                                </div>\n' +
                    '                                            </td>\n' +
                    '                                            <td>\n' +
                    '                                                <div class="product-content">\n' +
                    '                                                    <div class="product-name">\n' +
                    '                                                        <a href="#">'+name+'</a>\n' +
                    '                                                        <span>  </span>\n' +
                    '                                                    </div>\n' +
                    '                                                    <div class="product-price">\n' +
                    '                                                        <h5 class="price"><b> '+price+' TL </b></h5>\n' +
                    '                                                        <a href="/remove-from-cart/'+id+'" class="delete fa fa-close">  </a>\n' +
                    '                                                    </div>\n' +
                    '                                                </div>\n' +
                    '                                            </td>\n' +
                    '                                        </tr>')

            setTimeout(function(){
              window.location.href = '/';
            },200)

        })
      }else{
        alert('Lütfen beden seçiniz');
      }
    }else{
      var option = '-';
      $.post('/add-to-cart',{id:id,name:name,slug:slug,price:price,minimum:minimum,img:img,option:option},function(res){


              $('#cartBody').append('<tr>\n' +
                  '                                            <td>\n' +
                  '                                                <div class="product-media">\n' +
                  '                                                    <a href="#">  <img src="'+img+'" alt="'+name+'"></a>\n' +
                  '                                                </div>\n' +
                  '                                            </td>\n' +
                  '                                            <td>\n' +
                  '                                                <div class="product-content">\n' +
                  '                                                    <div class="product-name">\n' +
                  '                                                        <a href="#">'+name+'</a>\n' +
                  '                                                        <span>  </span>\n' +
                  '                                                    </div>\n' +
                  '                                                    <div class="product-price">\n' +
                  '                                                        <h5 class="price"><b> '+price+' TL </b></h5>\n' +
                  '                                                        <a href="/remove-from-cart/'+id+'" class="delete fa fa-close">  </a>\n' +
                  '                                                    </div>\n' +
                  '                                                </div>\n' +
                  '                                            </td>\n' +
                  '                                        </tr>')

          setTimeout(function(){
              window.location.href = '/';
          },200)

      })
    }


}
