function calculate(el){

    var total = 0;
    setTimeout(function(){
        if(el.hasClass('plus')){
            var adet = parseFloat(el.siblings('.cart-quantity').val())+1
        }else{
            var adet = parseFloat(el.siblings('.cart-quantity').val())-1
        }
        el.siblings('.cart-quantity').val(adet)
        var qty = $('.cart-quantity')
        qty.each(function(){

            var quantity = parseInt($(this).val());
            var price = parseFloat($(this).parents('.quantity').next().find('#price').val())

            total += quantity*price.toFixed(2);
        })

        $('#totalPrice').html(total.toFixed(2))
        $('#lastTotal').html(total.toFixed(2))



    },100)
    return false;
}
$('#toCheckout').click(function(e){
    e.preventDefault();
    var link = $(this).attr('href')
    submitForm(link);

})

function submitForm(link){
    var tr = $('tr')
    var arr = {};
    var i = 0;
    var tot = $(this).find('.tot').val();
    var max = $(this).find('.max').val();

    tr.each(function(){
        var id = $(this).attr('id');

        if(id != undefined && id != null && id != ''){
            var qty = $(this).find('.cart-item-quantity').find('.cart-quantity').val();

            arr[id] = qty;
            i++;
        }

    })


    $.post(link,arr, function(req, res){

      if(req === 'error'){
        window.location.href = '/sepet';
      }else{
        window.location.href = '/checkout';
      }



    })
}

jQuery(document).ready(function () {
  $('#user_method_guest').click(function () {
      $("#collapse-1-hed").addClass("collapsed");
      $("#collapse-1").removeClass("collapse in");
      $("#collapse-1").addClass("collapse");
      $("#kargo_islemleri").removeClass("collapsed");
      $("#collapse-2").removeClass("collapse in");
  });

  $('#user_method_register').click(function () {
      $("#collapse-1-hed").addClass("collapsed");
      $("#collapse-1").removeClass("collapse in");
      $("#collapse-1").addClass("collapse");
      $("#kargo_islemleri").removeClass("collapsed");
      $("#collapse-2").removeClass("collapse in");
  });
  $('#cargo_method_ptt','#cargo_method_aras','#cargo_method_mng').click(function () {
      $("#collapse-2-hed").addClass("collapsed");
      $("#collapse-2").removeClass("collapse in");
      $("#collapse-2").addClass("collapse");
      $("#odeme_islemleri").removeClass("collapsed");
      $("#collapse-3").removeClass("collapse in");
  });
  $("input[name='login_method']").click(function () {
      $("#postUser").removeAttr("disabled", false);
  });

  $('.field input').keyup(function() {

      var empty = false;
      $('.field input').each(function() {
          if ($(this).val().length == 0) {
              empty = true;
          }
      });

      if (empty) {
          $('#postAddress').attr('disabled', 'disabled');
      } else {
          $('#postAddress').removeAttr('disabled');
      }
  });

  $(document).ready(function() {
      $("#login-dialog").modal({
          show: false,
          backdrop: 'static'
      });
      $("#register-dialog").modal({
          show: false,
          backdrop: 'static'
      });
      $('#login-dialog').on('hidden.bs.modal', function () {
       location.reload();
     });
      $('#register-dialog').on('hidden.bs.modal', function () {
       location.reload();
     });
  });

  $('input[name="login_method"]').change(function() {
     if($(this).is(':checked') && $(this).val() == 'login') {
     		$('#login-dialog').modal('show');
     }else if($(this).is(':checked') && $(this).val() == 'register') {
       $('#register-dialog').modal('show');
     }
  });



});
