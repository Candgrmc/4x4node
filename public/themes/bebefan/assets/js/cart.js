function addToCart($this){
    var id = $this.attr('data-id')
    var price = $this.attr('data-price');
    var img = $this.attr('data-img');
    var name = $this.attr('data-name');
    $.post('/add-to-cart',{id:id,name:name,price:price,img:img},function(res){
        if(res){
            window.location.reload();
        }
    })
}