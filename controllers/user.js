var request = require('request');
module.exports.userInfo = function(id,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/crm/customer",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "ecommerce_id": process.env.ECOMMERCE_ID,
            "api_token": process.env.API_TOKEN,
            "customer_id" : id

        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            var data = JSON.parse(body).customer;
              
            callback(data);
        }
    })
}

module.exports.updateUser = function(sesuser,user,callback){
        request.post({
            "headers": { "content-type": "application/json" },
            "url": "https://lapia.net/api/v1/crm/customer/update",
            "body": JSON.stringify({
                "company_key": process.env.COMPANY_KEY,
                "company_secret": process.env.COMPANY_SECRET,
                "api_token": process.env.API_TOKEN,
                "customer_id" : sesuser.customer_id,
                "id": sesuser.id,
                "user_id": sesuser.user_id,
                "name": user.name,
                "surname": user.surname,
                "email": user.email,
                "mobil": user.mobil

            })
        }, function (error, response, body){

            if(!error && response.statusCode == 200){
                callback(body);
            }
        })
}

module.exports.updatePassword = function(sesuser,password,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/customer/password",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "api_token": process.env.API_TOKEN,
            "id": sesuser.id,
            "user_id": sesuser.user_id,
            "new_password":password
        })
    }, function (error, response, body){
        if(!error && response.statusCode == 200){
            var data = JSON.parse(body).status;
            callback(data);
        }
    })
}

module.exports.register = function(credentials,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/customer/register",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "email": credentials.email,
            "password": credentials.password,
            "name": credentials.name,
            "surname": credentials.surname,
            "phone": credentials.phone,
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            console.log(body);
            var data = JSON.parse(body);
            callback(data);
        }
    })
}
