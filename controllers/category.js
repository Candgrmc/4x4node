var request = require('request');
var chalk = require('chalk');
module.exports.getCategoryProducts = function(slug,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/categories/products",
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
          "api_token": process.env.API_TOKEN,
            "category_slug": slug
        })
    }, function (error, response, body){
        console.log(chalk.green('Category status: '+response.statusCode));
        if(!error && response.statusCode == 200){
            var data =JSON.parse(body);
            callback(data);

        }
    })
}
