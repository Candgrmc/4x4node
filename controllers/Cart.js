var request = require('request');
module.exports.completeOrder = function(products,orderInfo,callback){
    var postdata = {
      "company_key": process.env.COMPANY_KEY,
      "company_secret": process.env.COMPANY_SECRET,
      "api_token": process.env.API_TOKEN,
        "products" : products,
        "order_info" : orderInfo
    }
    if(orderInfo.user){
        postdata['user'] = orderInfo.user;
    }
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/crm/customer/orders/add",
        "body": JSON.stringify(postdata)
    }, function (error, response, body){
        console.log('Order- status: '+response.statusCode);
        if(!error && response.statusCode == 200){
            callback(body);
        }
    })
}
