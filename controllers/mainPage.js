
var request = require('request');
request.post({
    "headers": { "content-type": "application/json" },
    "url": "https://lapia.net/api/v1/warehouse/products",
    "body": JSON.stringify({
        "company_key": process.env.COMPANY_KEY,
        "company_secret": process.env.COMPANY_SECRET,
        "ecommerce_id": process.env.ECOMMERCE_ID,
        "warehouse_id": process.env.WAREHOUSE_ID,
        "api_token": process.env.API_TOKEN
    })
}, function (error, response, body){
    console.log(response.statusCode);
    if(!error && response.statusCode == 200){
        var data =JSON.parse(body).data;
        data;

        module.exports.products = JSON.stringify(data);
        module.exports.status = response.statusCode;
    }
})
