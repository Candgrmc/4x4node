var request = require('request');
var chalk = require('chalk');
module.exports.settings = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/ecommerce/detail/30",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "ecommerce_id": process.env.ECOMMERCE_ID,
            "api_token": process.env.API_TOKEN
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            console.log('Menu status: '+response.statusCode)
            var data =JSON.parse(body).ecommerce;
            callback(data);
        }
    })
}


request.post({
    "headers": { "content-type": "application/json" },
    "url": "https://lapia.net/api/v1/ecommerce/menus",
    "body": JSON.stringify({
      "company_key": process.env.COMPANY_KEY,
      "company_secret": process.env.COMPANY_SECRET,
      "ecommerce_id": process.env.ECOMMERCE_ID,
      "api_token": process.env.API_TOKEN
    })
}, function (error, response, body){

    if(!error && response.statusCode == 200){
        console.log(chalk.greenBright('Menu status: '+response.statusCode))
        var data =JSON.parse(body).menus;


        module.exports.menus = JSON.stringify(data);
    }
})

module.exports.categories = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/categories",
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
          "ecommerce_id": process.env.ECOMMERCE_ID,
          "api_token": process.env.API_TOKEN
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            console.log(chalk.green('Category status: '+response.statusCode))
            var data =JSON.parse(body).categories;


            callback(data);
        }
    })
}
