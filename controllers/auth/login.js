var request = require('request');
module.exports.log_me_in = function(crediantals,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/customer/login",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "email" : crediantals.email,
            "password" : crediantals.password,
        })
    }, function (error, response, body){
        console.log(body);
        if(!error && response.statusCode == 200){
            callback(body);
        }
    })
}
