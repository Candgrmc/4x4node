var request = require('request');

module.exports.updateAddress = function(user,address,callback){
    if(address.address_id){
        request.post({
            "headers": { "content-type": "application/json" },
            "url": "https://lapia.net/api/v1/crm/customer/address/update",
            "body": JSON.stringify({
                "company_key": process.env.COMPANY_KEY,
                "company_secret": process.env.COMPANY_SECRET,
                "customer_id": user.customer_id,
                "user_id" : user.user_id,
                "address_id" : address.address_id,
                "name" : address.name,
                "county" : address.county,
                "district" : address.district,
                "zipcode" : address.zipcode,
                "address" : address.address,
                "api_token": process.env.API_TOKEN
            })
        }, function (error, response, body){
            if(!error && response.statusCode == 200){
                var data =JSON.parse(body).status;

                callback(data);

            }
        })
    }else{
        request.post({
            "headers": { "content-type": "application/json" },
            "url": "https://lapia.net/api/v1/crm/customer/address/add",
            "body": JSON.stringify({
                "company_key": process.env.COMPANY_KEY,
                "company_secret": process.env.COMPANY_SECRET,
                "customer_id": user.customer_id,
                "user_id" : user.id,
                "name" : address.name,
                "country":'tr',
                "county" : address.county,
                "district" : address.district,
                "zipcode" : address.zipcode,
                "address" : address.address,
                "api_token": process.env.API_TOKEN
            })
        }, function (error, response, body){
            console.log('Address- status: '+body);
            if(!error && response.statusCode == 200){

                var data =JSON.parse(body).status;

                callback(data);

            }
        })
    }
}

module.exports.deleteAddress = function(user,address_id,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/crm/customer/address/delete",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "customer_id": user.customer_id,
            "user_id" : user.user_id,
            "address_id" : address_id,
            "api_token": process.env.API_TOKEN
        })
    }, function (error, response, body){
        console.log('Category- status: '+response.statusCode);
        if(!error && response.statusCode == 200){
            console.log('Address updated: '+body);
            var data =JSON.parse(body).status;

            callback(data);

        }
    })
}

module.exports.setPrimary = function(user,address_id,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/crm/customer/address/primary",
        "body": JSON.stringify({
            "company_key": process.env.COMPANY_KEY,
            "company_secret": process.env.COMPANY_SECRET,
            "customer_id": user.customer_id,
            "address_id" : address_id,
            "type" : 1,
            "api_token": process.env.API_TOKEN
        })
    }, function (error, response, body){
        console.log('Category- status: '+response.statusCode);
        if(!error && response.statusCode == 200){
            console.log('Address updated: '+body);
            var data =JSON.parse(body).status;
            callback(data);

        }
    })
}
