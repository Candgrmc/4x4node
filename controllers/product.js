var request = require('request');
var chalk = require('chalk');
module.exports.productDetail = function(slug,callback){

    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/product/"+slug,
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
          "ecommerce_id": process.env.ECOMMERCE_ID,
          "api_token": process.env.API_TOKEN,
        })
    }, function (error, response, body){
        console.log(chalk.greenBright('Product status: '+response.statusCode));
        if(!error && response.statusCode == 200){
            var data =JSON.parse(body);
            callback(data);
        }
    })
}

module.exports.products = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/products",
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
          "ecommerce_id": process.env.ECOMMERCE_ID,
          "warehouse_id": process.env.WAREHOUSE_ID,
          "api_token": process.env.API_TOKEN,
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            var data = JSON.parse(body);
            callback(data);
        }
    })
}

module.exports.showcase = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/products/showcase",
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
          "ecommerce_id": process.env.ECOMMERCE_ID,
          "warehouse_id": process.env.WAREHOUSE_ID,
          "api_token": process.env.API_TOKEN,
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            var data =JSON.parse(body);
            callback(data);
        }
    })
}

module.exports.discounts = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/discounts",
        "body": JSON.stringify({
            "company_key": "5",
            "company_secret": "5",
            "type": "2",
            "api_token": "QjZ90bnuIlzMl51jZvOxwNH7kKd636UdIBuAjS4tlVF4VjYsc8TZdjNtG2j4"
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            var data =JSON.parse(body);
            callback(data);
        }
    })
}

module.exports.onSaleProducts = function(callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/products",
        "body": JSON.stringify({
            "company_key": "5",
            "company_secret": "5",
            "ecommerce_id": "32",
            "warehouse_id": "6",
            "api_token": "QjZ90bnuIlzMl51jZvOxwNH7kKd636UdIBuAjS4tlVF4VjYsc8TZdjNtG2j4",
            "type" : 'discount'
        })
    }, function (error, response, body){

        if(!error && response.statusCode == 200){
            var data =JSON.parse(body);
            callback(data);
        }
    })
}
