var request = require('request');
var chalk = require('chalk');
module.exports.search = function(query,callback){
    request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://lapia.net/api/v1/warehouse/products/search/"+query.q,
        "body": JSON.stringify({
          "company_key": process.env.COMPANY_KEY,
          "company_secret": process.env.COMPANY_SECRET,
            "ecommerce_id": process.env.ECOMMERCE_ID,
            "api_token": process.env.API_TOKEN,
            "category_id" : query.kategori
        })
    }, function (error, response, body){
        console.log(chalk.green('Search status: '+response.statusCode));
        if(!error && response.statusCode == 200){
            var data =JSON.parse(body).data;
            callback(data);

        }
    })
}
