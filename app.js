var express     = require('express');
var https = require('https');
var engine      = require('ejs-locals');
var path        = require('path');
var request     = require('request');
var bodyParser  = require('body-parser');
var flash       = require('connect-flash');
var fs          = require('fs');
var session     = require('express-session');
var flash       = require('express-flash');
var toastr      = require('express-toastr');
var RedisStore  = require('connect-redis')(session);
var chalk       = require('chalk');
var dotenv      = require('dotenv').config();
var device      = require('express-device');
var app         = express();
app.set('trust proxy', 1);
var options = {
  key: fs.readFileSync('/home/pia/conf/web/ssl.4x4.lapia.net.key'),
  cert: fs.readFileSync('/home/pia/conf/web/ssl.4x4.lapia.net.crt')
};


app.use(session({
    cookie:{
      secure: true,
      maxAge:60000
    },
    store: new RedisStore(),
    secret: 'Pia Ecommerce',
    cookie: { maxAge: 3600000 },
    resave: false,
    saveUninitialized: true
}));
app.use(bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
}));
app.use(express.json());       // to support JSON-encoded bodies
app.use(express.urlencoded()); // to support URL-encoded bodies
app.use(flash());
app.use(toastr());
app.engine('ejs', engine);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.static("public"));
app.use(device.capture());
app.use(function (req, res, next) {
    res.locals = {
        siteTitle: "My Website's Title",
        pageTitle: "The Home Page",
        author: "Cory Gross",
        description: "My app's description",
        sesUser : req.session.user,
        sesCart: req.session.cart,
        url : req.url,
    };
    res.locals.toasts = req.toastr.render();
    next();
});
device.enableDeviceHelpers(app);
device.enableViewRouting(app);
    /***********\
    # ######### #
    #  ________ #
    #  |Routes| #
    #  -------- #
    # ######### #
  \**----------**/

app.get('/curl', function(req, res){
  var curl = require('./controllers/curl');
  console.log(curl);
});



app.get('/',function(req,res){
      var siteSettings = require('./controllers/globals');

      var product = require('./controllers/product');
      product.products(function(response){
      product.showcase(function(showcase){
      product.discounts(function(discounts){

          siteSettings.settings(function(settings){
              res.render('index',{
                  products : JSON.stringify(response.data),
                  showcase : JSON.stringify(showcase.data),
                  discounts : JSON.stringify(discounts.data),
                  best_sellers : JSON.stringify(response.best),
                  siteSettings : JSON.stringify(settings),
                  siteMenus : siteSettings.menus,
                  siteEmails: JSON.stringify(settings.email),
                  companyInfo: JSON.stringify(settings.company),
                  siteCategories : JSON.stringify(settings.categories),
                  brands : JSON.stringify(settings.brands),
                  title: 'Off Road Aksesuarları',
                  keywords: 'keyords',
                  description: 'desc',
              })
          })
      })
      })
      })
  });
  app.get('/sayfa/:slug',function(req,res){
      var siteSettings = require('./controllers/globals');
      siteSettings.settings(function(settings){

          res.render('pages/page',{
              siteSettings : JSON.stringify(settings),
              siteMenus : siteSettings.menus,
              siteEmails: JSON.stringify(settings.email),
              companyInfo: JSON.stringify(settings.company),
              pages : JSON.stringify(settings.pages),
              slug : req.params.slug,
              siteCategories : JSON.stringify(settings.categories),
              title: '',
              keywords: 'sayfa',
              description: 'say',
          })
      })
  })
app.get('/urun/:slug',function(req,res){
    delete require.cache[require.resolve('./controllers/globals')];
    var product = require('./controllers/product');
    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    product.productDetail(req.params.slug,function(response){
        var product = JSON.stringify(response);
        // var productsAlike = JSON.stringify(response.productsAlike);
        siteSettings.settings(function(settings){
            res.render('product/detail',{
                product : JSON.stringify(response),
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
                title: JSON.parse(product).product.meta_title,
                keywords: JSON.parse(product).product.meta_keywords,
                description: JSON.parse(product).product.meta_description,
            })
        })
        });
});

app.get('/kategori/:category',function(req,res){
    if(req.params.category){
        var category = require('./controllers/category');
        delete require.cache[require.resolve('./controllers/globals')];
        var siteSettings = require('./controllers/globals');

        category.getCategoryProducts(req.params.category,function(response){

            siteSettings.settings(function(settings){
                res.render('category/products',{
                    products : JSON.stringify(response.data),
                    siteSettings : JSON.stringify(settings),
                    siteMenus : siteSettings.menus,
                    siteEmails: JSON.stringify(settings.email),
                    companyInfo: JSON.stringify(settings.company),
                    siteCategories : JSON.stringify(settings.categories),
                    title: response.category.meta_title,
                    keywords: response.category.meta_keywords,
                    description: response.category.meta_description,

                })
            })

        });
    }

});

app.get('/kayit-ol',function(req,res){
  var category = require('./controllers/category');
  var siteSettings = require('./controllers/globals');
  siteSettings.settings(function(settings){

      res.render('auth/register',{
        siteSettings : JSON.stringify(settings),
        siteMenus : siteSettings.menus,
        siteEmails: JSON.stringify(settings.email),
        companyInfo: JSON.stringify(settings.company),
        siteCategories : JSON.stringify(settings.categories),
        title: 'Kayıt Ol',
        keywords: '4x4 led dünyası kayıt ol',
        description: '4x4 led dünyası kayıt olma sayfası',
      })
  })
});

app.post('/register',function(req,res){
    var user = require('./controllers/user');
    user.register(req.body,function(response){
        if(response.status == 'success'){
          req.toastr.success(response.response);
          res.redirect('/');
        }else{
          req.toastr.error(response.response);
          res.redirect('/');
        }
    })
});

app.get('/giris',function(req,res){

    var category = require('./controllers/category');
    var siteSettings = require('./controllers/globals');
    siteSettings.settings(function(settings){

        res.render('auth/login',{
          siteSettings : JSON.stringify(settings),
          siteMenus : siteSettings.menus,
          siteEmails: JSON.stringify(settings.email),
          companyInfo: JSON.stringify(settings.company),
          siteCategories : JSON.stringify(settings.categories),
          title: 'Giriş Yap',
          keywords: '4x4 led dünyası, offroad',
          description: '4x4 led dünyası giriş sayfası',
        })
    })
});

app.post('/login',function(req,res,next){
    if(req.session.user){
        res.redirect('/');
        next
    }else{
        var login = require('./controllers/auth/login');
        login.log_me_in(req.body,function(response){
            var status = JSON.parse(response).status;
            if(status == 'success'){
                var customer = JSON.parse(response).customer;
                req.session.user = customer;
                req.toastr.success('Giriş işleminiz başarılı.');
                backURL=req.header('Referer') || '/';
                res.redirect(backURL);
            }else{
              req.toastr.error('Giriş işleminiz başarısız!');
              backURL=req.header('Referer') || '/';
              res.redirect(backURL);
            }
        })
    }
});

app.get('/logout',function(req,res){
    req.session.user = null;
    req.toastr.info('Çıkış yaptınız.');
    res.redirect('/');
});

/*
    Shopping Cart
 */

app.post('/add-to-cart',function(req,res){
     if(req.session.cart){
         var Cart = req.session.cart;

         Cart.forEach(function(item){
           if(item.id == req.body.id && item.option == req.body.option){
                var bir = 1;
                 item.quantity = parseInt(item.quantity) + parseInt(bir);
                 req.session.cart = Cart;
                 req.toastr.info('Sepet Güncellendi.');
                 res.end('updated');
             }else{
                 var newProduct = req.body;
                 newProduct.quantity = 1;
                 Cart.push(newProduct);
                 req.session.cart = Cart;
                 req.toastr.success('Ürün başarıyla sepete eklendi.');
                 res.end('updated');
             }
         })
     }else{
         var Cart = [];
         var Product = req.body;
         Product.quantity = 1;
         Cart.push(Product);

         req.session.cart = Cart;
         req.toastr.success('Ürün başarıyla sepete eklendi.');
         res.end('created');
     }
 });

app.get('/remove-from-cart/:id',function(req,res){
    var Cart = req.session.cart;
    var bodyid = req.params.id;
    Cart.forEach(function(cart){
        var cartid = cart.id;
        if(cartid == bodyid){
            Cart.splice(cart,1)
        }
    })
    if(Cart.length){
        req.session.cart = Cart;
    }else{
        req.session.cart = null;
    }
    backURL=req.header('Referer') || '/';

    res.redirect(backURL);
})

app.get('/sepet',function(req,res){
    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    var cart = req.session.cart;
    if(cart == null){
      req.toastr.info('sepetnizde ürün mevcut değil.');
      return res.redirect('/');
    }
    siteSettings.settings(function(settings){
        res.render('cart/shopping-cart',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            title: 'Alışveriş Sepeti',
            keywords: 'alışveriş sepeti',
            description: 'alışveriş sepeti',
        })
    })
})

app.post('/checkout_update',function(req,res){
    var Cart = req.session.cart;
    var total = 0;
    var max   = 0;
    Cart.forEach(function(cart){
      var limit = cart.minimum;

      max += Math.max(limit);

      total += cart.price * cart.quantity;
        var id = cart.id;
        if(req.body[id]){
            cart.quantity = req.body[id];
        }
    })
    var tot = total.toFixed(2);
    if(tot < max){
      req.toastr.warning('ÖZEL İNDİRİMLİ ürünleri satın alabilmek için sepet tutarınız '+max+' TL olmalıdır.');
      res.send('error');

    }else{
      req.session.cart = Cart;
      res.send('success');

    }

})

app.get('/checkout',function(req,res){

    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    var cart = req.session.cart;
    if(cart == null){
      req.toastr.info('sepetnizde ürün mevcut değil.');
      return res.redirect('/');
    }
    siteSettings.settings(function(settings){
        res.render('cart/checkout',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            county : JSON.stringify(settings.county),
            payments:JSON.stringify(settings.payment_methods),
            title: 'Alışveriş Tamamlama',
            keywords: '4x4 led dünyası',
            description: '4x4 led dünyası',
        })
    })

})

app.get('/success',function(req,res){

    var siteSettings = require('./controllers/globals');
    var categories = siteSettings.categories;
    siteSettings.settings(function(settings){
        res.render('cart/success',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            county : JSON.stringify(settings.county),
            title: 'Sipariş Sonuç',
            keywords: 'sipariş tamam',
            description: 'sipariş tamam',
        })
    })

})

app.post('/complete_order',function(req,res){
    var cart = req.session.cart;
    var shop = require('./controllers/Cart');
    var orderInfo = req.body
    if(req.session.user){
        orderInfo['user'] = req.session.user;
    }

    shop.completeOrder(req.session.cart,orderInfo,function(response){
        status    = JSON.parse(response).status;
        response  = JSON.parse(response).response;
        if(status == 'success'){
          req.toastr.success(response);
          delete req.session.cart;
          res.redirect('success');
        }else{
          req.toastr.error(response);
          res.redirect('/');
        }
    })
})

/*
    Profil Routes
 */
 app.get('/profil',function(req,res){
   var user = require('./controllers/user');
   var category     = require('./controllers/category');
   var siteSettings = require('./controllers/globals');
   user.userInfo(req.session.user.customer_id,function(customer){
     siteSettings.settings(function(settings){

         if(req.session.user){

             res.render('profile/profile',{
               siteSettings : JSON.stringify(settings),
               siteMenus : siteSettings.menus,
               siteEmails: JSON.stringify(settings.email),
               companyInfo: JSON.stringify(settings.company),
               siteCategories : JSON.stringify(settings.categories),
               customer : customer,
               title: customer.fullname+' Profiliniz',
             })
         }else{
           res.redirect('/');
         }
     })
 });
 });

app.get('/profil/:page',function(req,res){
    var user = require('./controllers/user');
    delete require.cache[require.resolve('./controllers/globals')];
    var siteSettings = require('./controllers/globals');
    console.log(req.session.user.customer_id)
    if(req.params.page){
        switch(req.params.page){
            case 'account':
            user.userInfo(req.session.user.customer_id,function(customer){
                siteSettings.settings(function(settings){

                    res.render('profile/inc/account',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                        customer : customer,
                        title: customer.fullname+' Hesap Düzenle',
                        keywords: 'profil sayfası',
                        description: 'profil sayfası',
                    })
                })
                });
                break;
            case 'password':

                siteSettings.settings(function(settings){
                    res.render('profile/inc/password',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;
            case 'address':
                user.userInfo(req.session.user.customer_id,function(customer){
                    siteSettings.settings(function(settings){
                        res.render('profile/inc/address',{
                            siteSettings : JSON.stringify(settings),
                            siteMenus : siteSettings.menus,
                            siteEmails: JSON.stringify(settings.email),
                            companyInfo: JSON.stringify(settings.company),
                            siteCategories : JSON.stringify(settings.categories),
                            county : JSON.stringify(settings.county),
                            customer : customer
                        })
                    })
                });

                break;
            case 'address-edit':
                user.userInfo(req.session.user.customer_id,function(customer){
                    siteSettings.settings(function(settings){
                        res.render('profile/inc/address-edit',{
                            siteSettings : JSON.stringify(settings),
                            siteMenus : siteSettings.menus,
                            siteEmails: JSON.stringify(settings.email),
                            companyInfo: JSON.stringify(settings.company),
                            siteCategories : JSON.stringify(settings.categories),
                            customer : customer
                        })
                    })
                });

                break;
            case 'orders':

                siteSettings.settings(function(settings){
                    res.render('profile/inc/orders',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;
            case 'invoices':

                siteSettings.settings(function(settings){
                    res.render('profile/inc/invoices',{
                        siteSettings : JSON.stringify(settings),
                        siteMenus : siteSettings.menus,
                        siteEmails: JSON.stringify(settings.email),
                        companyInfo: JSON.stringify(settings.company),
                        siteCategories : JSON.stringify(settings.categories),
                    })
                })
                break;

        }
    }else{

    }
})

app.post('/update_address',function(req,res){
    var address = require('./controllers/address');
    address.updateAddress(req.session.user,req.body,function(response){
        if(response == 'success'){
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.get('/delete_address/:id',function(req,res){
    var address = require('./controllers/address');
    address.deleteAddress(req.session.user,req.params.id,function(result){
        if(result == 'success'){
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.get('/set_primary_address/:id',function(req,res){
    var address = require('./controllers/address');
    address.setPrimary(req.session.user,req.params.id,function(result){
        if(result == 'success'){
            backURL=req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
});

app.post('/update_user',function(req,res){
    var user = require('./controllers/user');
    user.updateUser(req.session.user,req.body,function(response){
        response = JSON.parse(response);
        if(response.status == 'success'){
            req.toastr.success('Profiliniz başarıyla güncellendi');
            backURL=req.header('Referer') || '/';
            req.session.user = response.customer;
            res.redirect(backURL);

        }
    })
})

app.post('/update_password',function(req,res){
    var user = require('./controllers/user');
    var password = req.body.password;
    var password_confirm = req.body.password_confirm;

    if(password == password_confirm){
        user.updatePassword(req.session.user,password,function(response){
            if(response == 'success'){
                req.toastr.success('Yeni parolanız başarıyla güncellendi.');
                backURL=req.header('Referer') || '/';
                res.redirect(backURL);
            }else{
                req.toastr.error('Parola güncellenemedi!.');
            }
        })
    }else{
        req.toastr.info('Parolalarınız uyuşmuyor.');
    }

})

app.post('/ara',function(req,res){
    delete require.cache[require.resolve('./controllers/globals')];
    var s = require('./controllers/search');
    var siteSettings = require('./controllers/globals');
    s.search(req.body,function(products){
        response = JSON.stringify(products);
        siteSettings.settings(function(settings){
            res.render('category/products',{
                products : response,
                siteSettings : JSON.stringify(settings),
                siteMenus : siteSettings.menus,
                siteEmails: JSON.stringify(settings.email),
                companyInfo: JSON.stringify(settings.company),
                siteCategories : JSON.stringify(settings.categories),
                title: 'Arama sonuçları',
                keywords: 'arama sonuçları',
                description: 'arama sonuçları',
            })
        })

    })
});

app.get('/admin', function(req,res){
  res.redirect('https://lapia.net/login');
});

app.get('/clear-cache',function(req,res){
    delete require.cache[require.resolve('./controllers/globals')];
    res.send('1');
});

app.get('*', function(req, res){
    var siteSettings = require('./controllers/globals');
    siteSettings.settings(function(settings){
        res.render('error/404',{
            siteSettings : JSON.stringify(settings),
            siteMenus : siteSettings.menus,
            siteEmails: JSON.stringify(settings.email),
            companyInfo: JSON.stringify(settings.company),
            siteCategories : JSON.stringify(settings.categories),
            title: 'Hata',
            keywords: 'hata',
            description: 'hata',
        })
    })
});

var server = app.listen(process.env.PORT, function(){
  console.log(chalk.red('Working on port:'+process.env.PORT))
});

https.createServer(options, app).listen(4002);
